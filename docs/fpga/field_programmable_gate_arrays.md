# FPGA Field Programmable Gate Array 

An FPGA is an device that has contains an array of configurable logic gates and can be programmed on-board after manufacturing and thus also re-programmed.
FPGA are becoming the primary source in embedded systems because of their balance between flexibility, performance and and rapid time to market. They can be used to program SoCs (System on one chip) and therefore offer an alternative to custom ICs (Integrated Circuits).

# Architecture

FPGAs are divided into small sub functions that can be programmed and connected in order to implement complex design.
The following parts of the architecture are essential components for FPGAs in general, but are described as implemented in the Cyclone V that it used in the DE10 Standard Boards. Some information may differ for other FPGAs.

## Logic Elements

FPGAs use ALMs (Adaptive Logic Modules) to implement the logic functions; they handle basic computing and storage elements.
Multiple ALM combine to a LAB (Logic Array Block) that controls its ALMs with control signals.
Each logic element is programmable and consist of a input lookup table (LUT), additional combinatorial logic (for example adders) and Flip-Flops (Register).

### LUT
A LUT is built out of SRAM bits to hold the memory, a set of multiplexers to determine the output and has 8-inputs (for Cyclone Devices). It can configured as multiple independent LUTs that share their inputs.

LUTs can operate in different modes: 

#### Normal Mode:
Normal mode allows two functions to be implemented in one ALM or a single function of up
to six inputs.
Up to eight data inputs from the LAB local interconnect are inputs to the combinatorial logic.
The ALM can support certain combinations of completely independent functions and various combinations of functions that have common inputs.

#### Extended LUT Mode:
In this mode, if the 7-input function is unregistered, the unused eighth input is available for register
packing.
Functions that fit into the template, as shown in the following figure, often appear in designs as “if-else”
statements in Verilog HDL or VHDL code.

#### Arithmetic Mode
The ALM in arithmetic mode uses two sets of two 4-input LUTs along with two dedicated full adders.
The dedicated adders allow the LUTs to perform pre-adder logic; therefore, each adder can add the output
of two 4-input functions.
The ALM supports simultaneous use of the adder’s carry output along with combinatorial logic outputs.
The adder output is ignored in this operation.

#### Shared Arithmetic Mode
The ALM in shared arithmetic mode can implement a 3-input add in the ALM.
This mode configures the ALM with four 4-input LUTs. Each LUT either computes the sum of three inputs
or the carry of three inputs. The output of the carry computation is fed to the next adder using a dedicated
connection called the shared arithmetic chain.

### Register

One ALM contains four programmable registers. Each register has the following ports:
- Data
- Clock
- Synchronous and asynchronous clear
- Synchronous load

Global logic, internal signals or GPIO Pins can drive the clock or clear control signals.

## Routings / Interconnects

LABs can connect to adjacent elements like other LABs, Memory Blocks or I/O-elements via a direct connection, row interconnects or column interconnects. The interconnections are using switch boxes and are programmable by the user / designer.

## Embedded Memory

The device contains two types of memory blocks:
 -  Memory blocks, that are dedicated memory blocks and ideal for large memory arrays and provide a large number of independent ports.
 - MLABs (Memory Logic Array Blocks) are a superset of LABs and optimized for implementation of shift registers for digital signal processing (DSP) applications, wide shallow FIFO-buffers, and filter delay lines.


# Security

Since FPGAs are very common in many critical fields (like for example the military), the security of these devices is crucial.
One effect of the architecture and flexibility of FPGAs is, that the possible vulnerabilities of the hardware is reduced, but the security has to be ensured by the software. This prevents problems like IP-theft caused by hardware realization, but increases the risk of flaws being introduced in the field.

## Security Problems

- Design-tool subversion: Very few checks to ensure that no malicious / flawed software is loaded onto the hardware
 * Software might do physical harm
 * Combination of trusted and untrusted software
- Composition:
 * You can only trust your final system as much as you trust your least-trusted design path
 * One core can snoop on another or tamper with it
 * Necessary to ensure security on different levels: device, board and network
- Bitstream protection:
 * IP theft (Solution: encryption, watermarks, fingerprinting)
 * Prevent bitstream from being uploaded on an update
- Handle Updates:
 * Automatic updated may introduce new flaws
 * Evaluate how much an update was tested and the history of security flaws

## Secure architecture as proposed by the UC Santa Barbara

- Memory Protection
 * Most embedded devices have a flat and unprotected memory
 * Possible Solution: Reference Monitor:
  + Self protecting
  + Its enforcement mechanisms can not be bypassed
  + Can be analyzed to ensure correctness and completeness
- Spatial Isolation: Physical protection
 * Isolate cores in spaces by controlling the layout function
 * Modularizing of the system
 * Use of buffers between the modules to separate them
- Tags: Metadata attached to pieces of the system data
 * Work as security labels
 * Can be analyzed to ensure the security
 * Use automatic functions to add tags instead of only privileged software
- Secure communication
 * Communication via shared memory: Use Reference Monitor
 * Direct connection: Static analysis to check the permissions
 * Communication via shared bus: Many threats like snooping and no solution yet that fulfills all requirements 


# Programming a FPGA

FPGA can be programmed either via a graphical program like LabVIEW FPGA, or with an HDL (Hardware descriptive language). Sancus uses Verilog to program the FPGA.
A tutorial to Verilog can be viewed on http://vol.verilog.com/ and a common tool to realize to written code is Quartus II (tutorial is provided in the file tut_quartus_intro_verilog.pdf).



# References

[1] Intel WP-01003-1.0: "FPGA Architecture" July 2006, ver. 1.0
[2] Altera CV-5V2: "Cyclone V - Device Handbook Volume 1" June 2008
[3] Intel CV-51001: "Cyclone V Device Overview" June 2008
[4] Ted Huffmire, Brett Brotherton, Timothy Sherwood, Ryan Kastner, Timothy Levin, Thuy Nguyen, and Cynthia Irvine. "Managing Security in the Design of FPGA Based Embedded Systems", IEEE Design & Test of Computers Vol 25 No. 6, November/December 2008. 
[5] teraIC "DE10-Standard User Manual" March 2018


yclone-v/cv_51001.pdf