# FPGAs

Sancus was developed and implemented on different FPGAs (Field Programmable Gate Arrays). The DE10 Standard Board uses the Cyclone V FPGA that is equipped with a HPS along with the actual FPGA.

| File | Description |
| --- | --- |
| [field_programmable_gate_arrays](field_programmable_gate_arrays.md) | General documentation on fpgas, their architecture and the issue of security|
| [Fpga_architecture_whitepaper](Fpga_architecture_whitepaper.pdf) | White paper by Altera on their fpga architecure|
| [Cyclone-5_Device-Datasheet](Cyclone-5_Device-Datasheet.pdf) | Cylone V Device Datasheet
| [Cyclone-5_Device-Handbook](Cyclone-5_Device-Handbook.pdf) | Cylone V Device Handbook
