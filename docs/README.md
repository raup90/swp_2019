# Directory: docs

### This directory contains all the documentation we collected/ wrote during the project

The documentation of our work and the collection of useful sources and knowledge are divided into the different topics, but may overlap or depend on each other since it is not possible to completely separate them.  
Our main goal was to include all the important knowledge without adding too many unnecessary documents and we provided an overview README for each folder to document the topics of the included files. 

| Dir/File | Description |
| - | - |
| [fpga](fpga/README.md) | General description of an FPGA |
| [DE10-standard](DE10-standard/README.md) | Overview and tutorials for our hardware |
| [Sancus](Sancus/README.md) | Whitepapers, instructions and documentation on Sancus |
| [openMSP430](openMSP430/README.md) | The openMsp430 project as a basis for Sanucs and our adaptions for the Altera DE10 Standard Board |
 
