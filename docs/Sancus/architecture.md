# Overview of the Sancus architecture

There are 4 keystones of the architecture:

1. Module isolation
2. Key management
3. Remote attestation and secure communication
4. Secure linking


## Module isolation

### Module layout

In sancus modules are representated as binary files with to sections:

- public text section
- protected data section

In the __public text section__ code and constants are stored and the __protected data section__ contains secret runtime data.
So only the module itself should have access to the data in the protected data section.

### Access rights enforcement

Modules are isolated using pc based memory access control 

- variable access rights
    + depending on the current pc
- isolation of data
    + only accessible from text section
- protection against misuse
    + e.g. rop
- enter module through single entry point (only first instruction of textsection is executable from outside)


PC based MAC table:

| from / to | Entry | Text | Protected | Unprotected |
| --------- | ----- | ---- | --------- | ----------- |
| Entry     | r-x   | r--  | rw-       | rwx         |
| Text      | r-x   | r-x  | rw-       | rwx         |
| Other     | r-x   | r--  | ---       | rwx         |


Layout of a node is stored in a protected storage area, there are new instructions to enable and disable protection

- `protect layout, SP`
    + enables isolation at layout and calculates K_{N,SP,SM}
- `unprotect`
    + disables isolation of current SM


## Key Management

### flexible, inexpensive way for secure communication

- establish a shared secret
  + between SP and one of its module SP
- use symmetric crypto
  + public-key is too expensive fo low-cost modules
- ability to deploy modules without IP intervening
  + after initial registration, that is

### key derivation scheme

In Sancus there's a hierarchical key derivation scheme:

   - IP is trusted party
     + able to derive all keys
   - every node N stores a key K_N
     + randomly generated
   - derived key based on SP ID
     + K_SP = kdf(K_N, SP)
   - derived key based on SM identity
     + K_SM = kdf(K_SP, SM)

The __node master key K_N__ and the __software module key K_SP__ for each protected module are stored in protected storage are.

There are new instructions to calculate the __software module key K_SP__:

- `protect layout, SP`
    + enables isolation at layout and calculates K_SM
- `unprotect`
    + disables isolation of current SM


## Remote attestation and secure communication


### secure communication

1. the SP sends some input and a nonce (for freshness guarantee) to the node containing its SM

2. SM receiving input, calculate some output O, sends back the output O and a _MAC_ based on the K_{N,SP,SM}, the nonce, input and output

    - MAC = message authentication code
    - The MAC is calculated by the new instruction:
        + `mac-seal`

3. SP receives the message and can recalculate the MAC (he knows the correct K_{N,SP,SM})

This provides trust in the authenticity of the messages, because only the SM can calculate the correct MAC.


### remote attestation

With remote attestation a SP wants to achieve the attestation of integrity, isolation and liveliness of a SM on the Node N.

With the implementation of the secure communication it's easy to achieve this goals:
     
- integrity and isolation is attested by the MAC
- liveliness by the nonce

So remote attestation is a subset of secure communication.


## Secure linking

Secure linking should enable efficient and secure local inter-module function calls.
If a Module calls a function of another module it has to verify the integrity of the other SM (is it the correct, isolated SM?).

Software Modules should be able to call function from Software Modules from other SP. So there's no shared secret.

Rely on protected local state.


### calculating MAC over own identity

Assume that Module A want to call Module B:

The idea is that Module A is deployed with a MAC of B's identity using A's key. This happens in an unprotected section since it is unforgeable.

When A wants to call B it starts with calculating the MAC of B's actual identity. If the calculated MACs match, B can safely be called.

There's a new instruction to verify the MAC's:

- `mac-verify`
- need ensurance that the MACs match and that the Module B is isolated

It's very expensive to calculate every time the Module's MAC, so there's a mechanism to calculate the MAC only once:

- only need to know if the same module still exists
    + initial verification
- Sancus assings unique ID's to modules
    + never reused within a boot cycle
- `mac-verify` returns the ID of the verified module
    + can be stored in protected section
- later calls can use a new instruction to check if the same module is still loaded:
    + `get-id`


