# Sancus

Sancus is a security architecture for IoT devices with the claim of supporting distributed applications on a shared infrastructure and having a zero-software Trusted Computing Base. It was first proposed 2013 at the USENIX Security conference by the KU Leuven.
Sancus already provides broad documentation on their architecture, API and examples on their Website [Sancus: Lightweight and Open-Source Trusted Computing for the IoT](https://distrinet.cs.kuleuven.be/software/sancus/index.php), thus we did not add all the documents that may contain useful information and rather focused on the basic knowledge and added our own insights.   
This folder mainly concern the general Sancus architecture and tools. Because Sancus may also be simulated with the sancus-sim, we provided information on that in [Repos](/Repos/) as well as a Docker image and Makefile in [Docker](/docker/).


| File | Description |
| --- | --- |
| [whitepapers/](whitepapers/) | Whitepapers on Sancus published by the KU Leuven |
| [architecture](architecture.md) | Overview of the Sancus architecture |
| [noorman_sec13_slides](noorman_sec13_slides.pdf) | Slides of the Sancus presentation 2013  |
| [c_asm_interface/](c_asm_interface/) | Documentation of the Sancus tools, API and usage|


