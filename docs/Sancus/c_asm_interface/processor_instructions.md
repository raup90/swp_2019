# Documentation of new processor instructions

## Files
* Verilog file for decoding debug signals, contains mapping from opcode to name:
    > [sancus-core/core/bench/verilog/msp_debug.v](https://github.com/sancus-pma/sancus-core/blob/master/core/bench/verilog/msp_debug.v#L440)
* ASM macro file, providing descriptive macro names for the new instructions
    > [sancus-core/core/sim/rtl_sim/src/sancus/sancus_macros.asm](https://github.com/sancus-pma/sancus-core/blob/master/core/sim/rtl_sim/src/sancus/sancus_macros.asm)
* C (header)file containing wrapper functions for many of the instructions
    > [sancus-compiler/src/sancus_support/sm_support.h](https://github.com/sancus-pma/sancus-compiler/blob/master/src/sancus_support/sm_support.h)

    > [sancus-compiler/src/sancus_support/sm_support.c](https://github.com/sancus-pma/sancus-compiler/blob/master/src/sancus_support/sm_support.c)
    (contains only `sancus_enable(_wrapped)`)

## TODO
1. Add link to where the instructions are implemented in the processor (modification)
2. Add reference to where the instructions are mentioned in the paper
3. Add description of what this instruction does

## unprotect
* Opcode: `0x1380`
* Name: `SM_DISABLE`
* Input registers:
    | Reg | Content |
    | --- | ------- |
    | r15 | Continuation (where to continue after leaving the software module) |
* Wrapper: [sm_support.h line 317](https://github.com/sancus-pma/sancus-compiler/blob/master/src/sancus_support/sm_support.h#L317)

## sancus_enable/ protect
* Opcode: `0x1381`
* Name: `SM_ENABLE`
* Input registers:
    | Reg | Content |
    | --- | ------- |
    | r9  | Tag (*)
    | r10 | Nonce (*)
    | r11 | ID of the vendor of this module |
    | r12 | SM Start address of public section
    | r13 | SM End address of public section
    | r14 | SM Start address of secret section
    | r15 | SM End address of secret section
    (*): Nur benutzt in `sancus_enable_wrapped()` ([sancus-compiler/src/sancus_support/sm_support.c](https://github.com/sancus-pma/sancus-compiler/blob/master/src/sancus_support/sm_support.c#L5)),
    0 in `sancus_enable()`
* Return value (in `r15`): ID of the enabled SM, 0 on error
* Wrapper: [sm_support.h line 304](https://github.com/sancus-pma/sancus-compiler/blob/master/src/sancus_support/sm_support.h#L304), 

## verify_address
* Opcode: `0x1382`
* Name: `SM_VERIFY_ADDR`
* Input registers:
    | Reg | Content |
    | --- | ------- |
    | r14 | Address of the SM to be verified |
    | r15 | Expected MAC |
* Return value (in `r15`): ID of the verified SM on success, 0 otherwise
* Wrapper: [sm_support.h line 346](https://github.com/sancus-pma/sancus-compiler/blob/master/src/sancus_support/sm_support.h#L346)

## verify_caller
* Opcode: `0x1383`
* Name: `SM_VERIFY_PREV`
* Input registers:
    | Reg | Description |
    | --- | ----------- |
    | r15 | Expected MAC |
* Return value (in `r15`): True if the verification succeeded
* Wrapper: [sm_support.h line 386](https://github.com/sancus-pma/sancus-compiler/blob/master/src/sancus_support/sm_support.h#L386)

## wrap_message
* Opcode: `0x1384`
* Name: `SM_AE_WRAP`
* Input registers:
    | Reg | Name | Description |
    | --- | ---- | ----------- |
    | r9  | (key) | Optional pointer to input key buffer of `SANCUS_KEY_SIZE` bytes; `NULL` to use the SM key of the executing module.
    | r10 | ad | Pointer to associated data buffer, mustn't be `NULL`
    | r11 | ad_end | Pointer to end of associated data buffer
    | r12 | (body) | Optional pointer to input buffer holding the plain text; `NULL` when only MAC over associated data is required.
    | r13 | (body_end) | End address of text buffer
    | r14 | (cipher) | Optional pointer to output buffer of `body_len` (`body_end - body`) bytes for the cipher text; cannot be `NULL` when `body` is non-zero.
    | r15 | tag | Pointer to output buffer of `SANCUS_TAG_SIZE` bytes for MAC over both associated data and plain text (if any); cannot be `NULL`;
* Return value (in `r15`): True if the wrapping succeeded
* Wrapper: [sm_support.h line 424](https://github.com/sancus-pma/sancus-compiler/blob/master/src/sancus_support/sm_support.h#L424)

## ???
* Opcode: `0x1385`
* Name: `SM_AE_UNWRAP`
* Input registers:
    | Reg | Name | Description |
    | --- | ---- | ----------- |
    | r9  | (key) | Optional pointer to input key buffer of `SANCUS_KEY_SIZE` bytes; `NULL` to use the SM key of the executing module.
    | r10 | ad | Pointer to associated data buffer, mustn't be `NULL`
    | r11 | ad_end | Pointer to end of associated data buffer
    | r12 | cipher | Pointer to cipther text buffer of `cipher_len` (`cipher_end - cipher`) bytes for
    | r13 | cipher_end | Pointer to end of cipher text buffer
    | r14 | body | Pointer to output buffer of `cipher_len` bytes for unwrapped plain text
    | r15 | tag | Pointer to output buffer of `SANCUS_TAG_SIZE` bytes for MAC over both associated data and plain text (if any); cannot be `NULL`;
* Return value (in `r15`): True if the unwrapping succeeded
* Wrapper: [sm_support.h line 471](https://github.com/sancus-pma/sancus-compiler/blob/master/src/sancus_support/sm_support.h#L471)

## get_id
* Opcode: `0x1386`
* Name: `SM_ID`
* Input registers:
    | Reg | Name | Description |
    | --- | ---- | ----------- |
    | r15 | addr | Address to examine |
* Return value (in `r15`): Sancus ID of the module loaded at `addr`
* Wrapper: [sm_support.h line 537](https://github.com/sancus-pma/sancus-compiler/blob/master/src/sancus_support/sm_support.h#L537)

## get_caller_id
* Opcode: `0x1387`
* Name: `SM_CALLER_ID`
* Input registers: None
* Return value (in `r15`): Sancus ID of the previously executing module (the module that entered the currently executing module)
* Wrapper: [sm_support.h line 565](https://github.com/sancus-pma/sancus-compiler/blob/master/src/sancus_support/sm_support.h#L565)

## set stack overflow guard address
* Opcode: `0x1388`
* Name: `SM_STACK_GUARD`
* Input registers:
    | Reg | Name | Description |
    | --- | ---- | ----------- |
    | r15 | addr | Address of stack overflow guard, `0` to disable |
* Wrapper: No C wapper, but ASM macro at [sancus_macros.asm line 14](https://github.com/sancus-pma/sancus-core/blob/master/core/sim/rtl_sim/src/sancus/sancus_macros.asm#L14)