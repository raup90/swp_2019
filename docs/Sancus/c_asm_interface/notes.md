# Notes

## Inline-Assembly Syntax
Nutzt AT&T-Syntax
```c
asm ( assembler code 
    : output operands          /* optional */
    : input operands           /* optional */
    : clobbered registers      /* optional */
    );
```
### Beispiel:
```c
int res;
int a = 5;
int b = 10;
asm("mov %1, r9\n\t"  // move param 1 into r9
    "mov %2, r15\n\t"
    "add r9, r15\n\t"
    "mov r15, %0"
    : "=m"(res)
    : "m"(a), "m"(b)
    : "9", "15");
```
* `m`(a) bedeuted, dass direkt auf die Speicherstelle `a` zugegriffen wird (anstatt dass a evtl. schon in einem Register liegt) [https://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html]
    > we use memory constraints for all operands because otherwise LLVM's
    > register allocator messes up and uses some of the clobbered registers


## Links/ files to examine
* [omsp_frontend.v line 611](../../Repos/sancus-core/core/rtl/verilog/omsp_frontend.v#L611)
* [openMSP430_defines.v line 647](../../Repos/sancus-core/core/rtl/verilog/openMSP430_defines.v#L647)
* [omsp_execution_unit.v line 186](../../Repos/sancus-core/core/rtl/verilog/omsp_execution_unit.v#L186)
  * [sancus module control, line 541](../../Repos/sancus-core/core/rtl/verilog/omsp_execution_unit.v#L541)
* [omsp_spm_control.v](../../Repos/sancus-core/core/rtl/verilog/omsp_spm_control.v)
* [omsp_spm.v](../../Repos/sancus-core/core/rtl/verilog/omsp_spm.v) - Looks like the definition of a software module