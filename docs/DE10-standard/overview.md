# Altera DE10-Standard - files, tools and manuals

This file should give an overview about the contents on the delivered CD for the _Altera DE10-Standard board_, the important manuals on the CD, the necessary tools for developing with this board and the used files from Sancus developer in the __sancus-core__ repository.

For detailed installation instruction of the tools please refer to [README.md](README.md).


## CD Contents

First, we brief about the important contents on the provided CD. The main focus is to show which of the given manuals are usefull to getting started with the _Altera DE10-Standard board_.

### Tools

#### System Builder

Along with the DE10 Board, Altera provided the DE10-Standard_CD with sample demonstrations, manuals and the SystemBuilder tool.  
The SystemBuilder generates the files to start a project for the FPGA with the main files being:

File               |  Description
-------------------|---------------------------------------
DE10_Standard.qpf  | Project File for Quartus 2
DE10_Standard.qsf  | All assignments for the pins and names for our specific FPGA
DE10_Standard.sdc  |
DE10_Standard.v    | Main Module with all Inputs & Outputs for the FPGA to write the actual code

#### Quartus 2

Quartus 2 compiles the code and enables the user to flash it onto the FPGA.  
More information on how to connect to the DE10 Standard Board and flash a file: [Altera DE10-Standard - getting started](docs/DE10-standard/README.md)  

#### IP Catalog: MegaWizard Plugin

__TODO__


### Manuals

To get started with the DE10-Standard board follow the manuals listed in __[DE10_Standard_Learning_Roadmap.pdf](../../tools/DE10_contents/DE10-Standard_v.1.3.0_SystemCD/Manual/DE10_Standard_Learning_Roadmap.pdf)__:

1. [DE10-Standard_QSG.pdf](../../tools/DE10_contents/DE10-Standard_v.1.3.0_SystemCD/Manual/DE10-Standard_QSG.pdf)

   Quick Start Guide for _Altera DE10-Standard_ board.

   Contents of this guide:
     - Software installation: refer to [DE10-Standard_Getting_Started_Guide.pdf](../../tools/DE10_contents/DE10-Standard_v.1.3.0_SystemCD/Manual/DE10-Standard_Getting_Started_Guide.pdf)
     - Starting your first FPGA Design
     - LinuxBPS (Board Support Package)
     - Starting your first HPS Design


2. [DE10-Standard_Getting_Started_Guide.pdf](../../tools/DE10_contents/DE10-Standard_v.1.3.0_SystemCD/Manual/DE10-Standard_Getting_Started_Guide.pdf)

   Overview of the hardware and software setup including step-by-step procedures from installing the necessary software tools to using the _DE10-Standard_ board.

   Main topics:
     - Software installation: __Quartus Prime__ and __SoC EDS__
   	 - Development board setup: Power up the DE10-Standard
   	 - Perform FPGA system test: Download an FPGA SRAM Objective File (.sof)
   	 - Run Linux on the DE10-Standard board


   Software:
    - __Altera Quartus II Software__
    
        primary FPGA development tool used to create reference designs along with the NIOS II soft-core embedded processor integrated development environment

    - __Altera SoC Embedded Design Suite__

        contains development tools, utility programs, run-time software and application examples to enable embedded development on the Altera SoC hardware platform. User can use the Altera SoC EDS to develop firmware and application software. 


3. [DE10-Standard_My_First_Fpga.pdf](../../tools/DE10_contents/DE10-Standard_v.1.3.0_SystemCD/Manual/DE10-Standard_My_First_Fpga.pdf)

   This manual provides comprehensive information that will help to understand how to create a FPGA design an run it on the _DE10-Standard development board_. The sections in this document provide a quick overview of the desgin flow and explain what is needed to get started.

4. [DE10-Standard_My_First_HPS.pdf](../../tools/DE10_contents/DE10-Standard_v.1.3.0_SystemCD/Manual/DE10-Standard_My_First_HPS.pdf)

   This tutorial provides comprehensive information that will help to understand how to create a C-language software design and run it on the ARM-included _DE10-Standard development board_.

5. [DE10-Standard_Control_Panel.pdf](../../tools/DE10_contents/DE10-Standard_v.1.3.0_SystemCD/Manual/DE10-Standard_Control_Panel.pdf)

   The scope of this lecture is to help users understand and learn how to build the board utility "Control Panel" on the _DE10-Standard develpment board_ in a step-by-step fashion.


## Sancus repository: sancus-core

This section gives some information about the [sancus-core](https://github.com/sancus-pma/sancus-core) repository to get a quick overview which of the directories and files are relevant to implement a new FPGA for Sancus.


### FPGA section from Sancus Homepage

Here is an extract from the [Sancus homepage](https://distrinet.cs.kuleuven.be/software/sancus/install.php), describing their used FPGA and including features:

> An ISE project for synthesizing Sancus for a XuLA2-LX25 FPGA on a StickIt!-MB board can be found in $SOURCE_ROOT/fpga/xula2-stickit. We also provide a pre-compiled BIT file as well as an MCS file for loading into the SPI/Flash. By default, Sancus is synthesized with the following features:
>
>    - Support for 8 concurrent protected modules.
>    - 64-bit keys
>    - 48KB of program memory.
>    - 10KB of data memory.
>    - Debug interface.
>    - UART interface.
>


### Directory structure

A quick overview about the directory structure of the sancus-core repository (extract from the file _[sancus-core/doc/html/files_directory_description.html](https://github.com/sancus-pma/sancus-core/doc/html/files_directory_description.html)_)




## Links

- [Sancus homepage](https://distrinet.cs.kuleuven.be/software/sancus/install.php)
- [sancus-core repository](https://github.com/sancus-pma/sancus-core)
