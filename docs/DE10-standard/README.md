# Working with the DE10-Standard board

The Altera DE10-Standard is a FPGA/SoC development board by Terasic equipped with a Cyclone V FPGA and a dual-core Cortex-A9 hard processor system (HPS). 
[DE10-Standard](https://www.terasic.com.tw/cgi-bin/page/archive.pl?Language=English&No=1081)

| File | Description |
| --- | --- |
| [setup](setup.md) | Getting started (board and software setup) |
| [overview](overview.md) | Overview over relevant files (board CD and repositories) |
| [run_project](run_project.md) | Instructions how to compile/download a project to the board |
| [tutorial_quartusii_intro_verilog](tutorial_quartusii_intro_verilog.pdf) | Quartus Prime Introduction using Verilog Designs |

Board layout:
![DE10-Standard board layout image](img/Layout.jpg "DE10-Standard board layout")
