# Loading and compiling a Quartus II project

1. Open the project: _File > Open Project..._ (or _Strg+J_) > Select and open the `.qpf` file

2. Compile the project: _Processing > Start Compilation_ (or _Strg+L_)

![Screenshot: Compilation finished](img/screen_compilation_finished.png)

3. Next, open the Programmer: _Tools > Programmer_ (or click the button in the "Standard"-toolbar)

![Screenshot: Toolbar Standard](img/screen_toolbar_programmer.png)
![Screenshot: Programmer](img/screen_programmer.png)

4. Open the "Hardware Setup..." dialog and select the board (double-click it's entry)

![Screenshot: Programmer](img/screen_hardware_setup.png)

5. Back in the Programmer, click on "Auto Detect". In the following dialog, select "5CSXFC6D6"

![Screenshot: Auto Detect](img/screen_auto_detect_device.png)

6. Back in the Programmer, you should see 2 devices now: the HPS and the FPGA show up as individual devices.
Click on the FPGA (either it's icon or the entry in the list) and click on "Change File...")

![Screenshot: Change File](img/screen_auto_change_file.png)

Select the generated `.sof` file and open it.
In the list showing the 2 devices, make sure the "Program/Configure"-checkbox for the FPGA is checked.
7. Lastly, click on "Start". Our design now gets downloaded to the board.

![Screenshot](img/screen_auto_start_download.png)