# Altera DE10-Standard - getting started

##  First contact with the devolpment board

__Performing Power-on Test: FPGA Configuration__

1. Set the `MSEL[4:0]=10010` in __Fast AS Mode__
2. Connect the power adapter to the power jack on the _DE10-Standard_, then press the power button to power it up
3. All the FPGA user LEDs will be flashing and 7-segment displays will be counting
4. Connect a VGA monitor to the D-sub connector(J9) and the VGA monitor will display the board image

## Software installation: Quartus Prime and SoC EDS

This section explains how to install the following software:

- __Altera Quartus II Software__:

    primary FPGA development tool used to create reference designs along with the NIOS II soft-core embedded processor integrated development environment

- __Altera SoC Embedded Design Suite__:

    contains development tools, utility programs, run-time software and application examples to enable embedded development on the Altera SoC hardware platform. User can use the Altera SoC EDS to develop firmware and application software. 

### Installing Quartus II software

The Altera Complete Design Suite provides the necessary tools used for developing hardware and software solutions for Altera FPGAs. The Quartus II software is the primary FPGA development tool used to create reference designs along with the NIOS II soft-core embedded processor integrated development environment.

#### ArchLinux

ArchLinux users can install the Quartus II software from the [AUR](https://aur.archlinux.org/packages/quartus-free/). For the Cyclone V device support you have to patch the PKGBUILD. Follow the explained instructions.

First install Quartus II Software (includes NIOS II EDS):
```bash
yay -S quartus-free
```

Download the Cyclone V device support:
```bash
git clone https://aur.archlinux.org/quartus-lite-cyclonev.git
cd quartus-lite-cyclonev.git
```

Now download the provided file [quartus-free.patch](quartus-free.patch) into this directory and patch/install the package:
```bash
patch PKGBUILD quartus-free.patch
makepkg -si
```

#### Ubuntu

##### Intel® FPGA Software prerequisites:

According to the Intel Documentation, Intel® FPGA Software requires the following prerequisites to be installed:
libc6:i386, libncurses5:i386, libxtst6:i386, libxft2:i386, libc6:i386, libncurses5:i386, libstdc++6:i386, libc6-dev-i386 libxft2, lib32z1, lib32ncurses5, lib32bz2-1.0, and libpng12 libraries

Ubuntu 18.04 also requires libqt5xml5 and liblzma-dev libraries.

__INFO: the libpng12 can not be installed with apt-get and has to be installed from https://packages.ubuntu.com/xenial/amd64/libpng12-0/download__ 

##### Downloading the Quartus II Software from the Intel Website

The Quartus II Software can be downloaded from http://fpgasoftware.intel.com/?edition=lite. Downloading from the Intel Download Center requires an Account that can be created for free, and the Lite-Version does not require a licence.
After downloading the archive, open the extracted folder and run setup.sh.

The Device Support for Cyclone V can either be installed together with the Quartus II Software, or separately on:
    Quartus Prime Lite Edition -> Individual Files -> Cyclone V device Support 
If the device support was downloaded separately, it's not installed yet and only saved as a .qdz file. In order to install it, open Quartus II
    > Tools > Install devices

##### Install Programming Cable Drivers 

The FPGA can only be detected by Quartus II, if the correct Programming Cable Drivers are installed:

Create a file named /etc/udev/rules.d/51-usbblaster.rules.
In the file, write the following lines: 
```
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="09fb", ATTRS{idProduct}=="6001", MODE="0666"
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="09fb", ATTRS{idProduct}=="6002", MODE="0666"
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="09fb", ATTRS{idProduct}=="6003", MODE="0666"

    SUBSYSTEMS=="usb", ATTRS{idVendor}=="09fb", ATTRS{idProduct}=="6010", MODE="0666"
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="09fb", ATTRS{idProduct}=="6810", MODE="0666"
```

To add the correct group to the user run the following commands and re-log / restart the computer:
```bash
sudo addgroup plugdev
sudo usermod -aG plugdev <USER>
```

### Installing Altera Soc Embedded Design Suite

The Altera SoC Embedded Design Suite (EDS) contains development tools, utility programs, run-time software and application examples to enable embedded development on the Altera SoC hardware platform. User can use the Altera SoC EDS to develop firmware and application software. Users can download the software from the [Altera webpage](http://url.terasic.com/soceds_download)

Download and install instructions:
1. Download SoC EDS software into a temporary directory.
2. Run the SoCEDSSetup-19.1.0.670-linux.run file.

__INFO:__ To download the SoC EDS software you need an Intel account.

__INFO:__ After success installing the DS-5, user can select the "Generate a 30-day evaluation license for DS-5 Ultimate Edition".

__Tipps for installation:__

The best way to install the DS-5 software is to execute the SoCEDSSetup-19.1.0.670-linux.run file without `sudo` permissions and do the necessary post-installations afterwards. Otherwise the installer puts the installation in __/root/intelFPGA/19.1/__ and you always have to run this program as root. For installation just type:
```bash
./SoCEDSSetup-19.1.0.670-linux.run
```

During the installation the installer opens a new `xterm` window executinga shell script. When you see the warning, that you don't have root privilegues pass this option:
```bash
=============================================
Welcome to the Installer for Arm DS-5 v5.29.2
=============================================
--- Host target check...[x86_64]
This installation has a post install step that requires root privileges
The post install stage performs the following functions:
- Installation of USB drivers for RealView ICE and DSTREAM hardware units
- Set default toolkit selection

 (A) Abort the installation and try again as a user with root privileges (Recommended)
 (B) Continue with the installation and skip stages that require root privileges
     (execute "run_post_install_for_Arm_DS-5_v5.29.2.sh" afterwards as root to gain full functionality)

Please answer with one of: 'A/a' or 'B/b'
Enter option: [default: A] B
```

After successfull installation you have to do the post-installation steps as root:
```bash
cd ~/intelFPGA/19.1/embedded/ds-5
sudo ./run_post_install_for_Arm_DS-5_v5.29.2.sh
```

This installs the necessary usb driver `USB-Blaster`

## Perform FPGA system test

Follow the instructions from the [DE10-Standard_My_First_Fpga.pdf](../../tools/DE10_contents/DE10-Standard_v.1.3.0_SystemCD/Manual/DE10-Standard_My_First_Fpga.pdf):

Downloading an FPGA SRAM Objective File (.sof) can be done by configuring the FPGA in the Fast AS Mode as described at the beginning. The remaining switch is on '0' to enable configuring the FPGA, therefore the final configuration should be '010010'.

Connect to the USB II Blaster via cable and open the Quartus II Programmer.

The first step is setting up the Hardware for the Quartus II:
    > Tools > Programmer > Hardware Setup
The dropdown "Currently selected hardware" should display the FPGA as a option, if not it is most likely caused by missing Programming Cable Drivers, please double check the installation.
The device may also be detected automatically, if there are multiple devices with a shared JTAG-ID, the selected device should be 5CSXFC6D6.

After adding the device, the JTAG-Chain should display two entries: the FPGA and the HPS device.

In order to download a file to the FPGA, click on that entry > Add File > select the file.
The DE10-Standard_SystemCD provides test-files in the directory \Demonstration, one simple file is provided in \Demonstration\FPGA\my_first_fpga\my_first_fpga.sof.
Click “Program/Configure” check box, and then click “Start” button to download .sof file into the FPGA.

## Links

- [CD Contents](http://de10-standard.terasic.com/cd)
- [Quartus II software](http://url.terasic.com/quartus_download)
- [AUR: quartus-lite-cyclonev](https://aur.archlinux.org/packages/quartus-lite-cyclonev/)
- [License for Quartus II](https://www.altera.com/support/support-resources/download/licensing.html)
- [Altera SoC Embedded Design Suite](http://url.terasic.com/soceds_download)
