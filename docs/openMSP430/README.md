# OpenMsp

OpenMsp430 is a open source project written in Verilog by Oliver Girad and simulates the Texas Instruments MSP430 microcontroller on FPGAs and ASICs. Sancus adds different features to it in order to implement the isolation of software modules, but did not modify the original project.   
Since there was no adaption provided for our Altera DE10 Standard Board, one main part of our work was the attempt to run OpenMsp430 on our hardware.

| File | Description |
| --- | --- |
| [OpenMSP430_2017](OpenMSP430_2017.pdf) | Documentation on openMSP430 by Oliver Girad |
| [OpenMsp430_src_overview](OpenMsp430_src_overview.md) | Overview of the repository, source code and important files|
| [OpenMsp_on_DE10](OpenMsp_on_DE10.md) | Adaptions to implement openMSP430 on the Altera DE10 Standard Board
| [Differences_DE1_DE10](Differences_DE1_DE10.md) | Comparison of the components used by openMSP430 and provided by the Cyclone V FPGA on the DE10 Board|
