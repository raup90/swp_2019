\documentclass[ucs,9pt]{beamer}

% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice.
%
% Modified by Tobias G. Pfeiffer <tobias.pfeiffer@math.fu-berlin.de>
% to show usage of some features specific to the FU Berlin template.

% remove this line and the "ucs" option to the documentclass when your editor is not utf8-capable
\usepackage[utf8x]{inputenc}    % to make utf-8 input possible
\usepackage[english]{babel}     % hyphenation etc., alternatively use 'german' as parameter

\include{fu-beamer-template}  % THIS is the line that includes the FU template!

\usepackage{arev,t1enc} % looks nicer than the standard sans-serif font
% if you experience problems, comment out the line above and change
% the documentclass option "9pt" to "10pt"

% image to be shown on the title page (without file extension, should be pdf or png)
\titleimage{Altera_StratixIVGX_FPGA}

\title[Sancus - Low-Cost Security Architecture] % (optional, use only with long paper titles)
{Sancus}

\subtitle
{Low-Cost Security Architecture for IoT Devices}

\author[Rau, Oehler, Frank] % (optional, use only with lots of authors)
{P.~Rau \and A.~Oehler \and E.~Frank}
% - Give the names in the same order as the appear in the paper.

\institute[FU Berlin] % (optional, but mostly needed)
{Freie Universität Berlin}
% - Keep it simple, no one is interested in your street address.

\date[WS 2019/20] % (optional, should be abbreviation of conference name)
{Softwareproject: Telematics}
% - Either use conference name or its abbreviation.
% - Not really informative to the audience, more for people (including
%   yourself) who are reading the slides online

\subject{Softwareproject: Telematics WS 2019/2020}
% This is only inserted into the PDF information catalog. Can be left
% out.

% you can redefine the text shown in the footline. use a combination of
% \insertshortauthor, \insertshortinstitute, \insertshorttitle, \insertshortdate, ...
\renewcommand{\footlinetext}{\insertshortinstitute, \insertshorttitle, \insertshortdate}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
%% \AtBeginSubsection[]
%% {
%%   \begin{frame}<beamer>{Outline}
%%     \tableofcontents[currentsection,currentsubsection]
%%   \end{frame}
%% }

% This sets the 2nd level bullets to dashes globally
\setbeamertemplate{itemize subitem}{$-$}

\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
  % You might wish to add the option [pausesections]
\end{frame}

\section{Sancus 2.0 review}

\subsection{What was Sancus again?}
\begin{frame}{What was Sancus again?}
  \begin{itemize}
  \item
    low-cost security architecture for IoT devices
  \item
    zero software TCB
  \item
    build on an extended OpenMSP430 Core
  \item
    generic model with
    \begin{enumerate}[-]
    \item
      Infrastructure Provider
    \item
      Software Provider
    \item
      Software Modules
    \end{enumerate}
  \item Goals:
    \begin{enumerate}[-]
    \item
      strong isolation between software modules
    \item
      secure communication and attestation
    \item
      counteracting attackers with full control over the infrastructural software
      \end{enumerate}
  \end{itemize}
\end{frame}

\section{Status Quo}
\subsection{Status Quo - Connection to the Hardware}
\begin{frame}{Status Quo - Connection to the Hardware}
  \begin{itemize}
  \item
    Configure the FPGA in Fast AS Mode: MSEL[4:0]=10010
  \item
    Download and install Intel Quartus II - Lite
  \item
    Add Device Support
  \item
    Install cable drivers
  \item
    Install Intel SoC FPGA Embedded Development Suite
  \item Download a SRAM Objective File (.sof) File to the FPGA:
    \begin{enumerate}[-]
    \item
      Hardware setup
    \item
      Downloading the File to the FPGA
      \end{enumerate}
  \end{itemize}
\end{frame}

\section{Altera DE1 Board vs. Altera DE10-Standard Board}
\begin{frame}{Altera DE1 Board vs. Altera DE10-Standard Board}
  \begin{itemize}
  \item Change Cyclone II $\rightarrow$ Cyclone V
  \item Location/Pin Assignments in Quartus .qsf file
  \item OpenMSP430
    \begin{itemize}
    \item FPGA processor implementation Sancus modifies
    \item DE10-Standard board not supported
    \item fork sancus-core
    \item \emph{/rtl/verilog/OpenMSP430\_fpga.v}: contains board main module
    \item \emph{/core/synthesis/altera/src/megawizard/}: auto generated memory modules
    \end{itemize}
  \item DE1 board uses 24 MHz and "external" clock, DE10 has 50 MHz clocks
  \item DE10 doesn't seem to have a UART interface (used for debug output)
  \end{itemize}
\end{frame}

\section{Sancus repositories}

\subsection{overview}
\begin{frame}{Sancus repositories - overview}
  \begin{itemize}
  \item
    sancus-main\par
    $\Rightarrow$ building the whole project, \texttt{Makefile} for pulling all repositories
  \item
    sancus-compiler\par
    $\Rightarrow$ building the toolchain, e. g. \texttt{sancus-cc}, \texttt{sancus-ld} and \texttt{sancus-crypto}
  \item
    sancus-support\par
    $\Rightarrow$ untrusted support software and device drivers
  \item
    sancus-core\par
    $\Rightarrow$ minimal OpenMSP430 hardware extensions for isolation and attestation
  \item
    sancus-examples\par
    $\Rightarrow$ examples and test suite
  \end{itemize}
\end{frame}


\subsection{focus: sancus-core}
\begin{frame}{Sancus repositories - sancus-core}
  \textbf{Minimal OpenMSP430 hardware extensions for isolation and attestation}
  % The following outlook is optional.
  \vskip0pt plus.5fill
  This repository provides source code and binary packages for the official Sancus 2.0 release: simulator, compiler and runtime support libraries. \par
  There are 4 relevant directories:
  \begin{itemize}
  \item
    core\par
    openMSP430 Core top level directory %, containing benchmarks, diverse documentations, RTL sources (verilog files and peripherals directory), simulations directory (RTL simulations scripts) and synthesis (synthesis setup for area and speed analysis)
  \item doc\par
    diverse documentation, e.g. about the openMSP430 microcontroller with manual and configuration for usage
  \item fpga\par
    openMSP430 FPGA Projects top level directory %, containing the supported FPGAs with benchmarks, documentation and rtl sources
  \item tools\par
    openMSP430 Software Development Tools top level directory %, containing the main TCL scripts, Common libraries and GDB Proxy server main project directory
  \end{itemize}
\end{frame}



\section{Our Repository}

\subsection{Structure}

\begin{frame}{Our Repository - Structure}
  \begin{itemize}
    \item docker\par
      directory for building the toolchain using Docker
    \item docs\par
      all (external) documentations and user manuals
    \item presentation\parallel
      our results for the presentation of this project
    \item Repos\par
      directory for all repositories from Sancus.\par
      There are also installation instructions for building the Sancus toolchain on your host system.
    \item src\par
      directory for our source code, i. e. software modules to deploy
    \item tools\par
      external tools needed for this project, i. e. the CD contents from our development board
    \item wiki \par
      wiki for further documentation, i. e. Sancus architecture overview, Sancus repositories overview, ...
  \end{itemize}
\end{frame}


\subsection{Documentation and Wiki}
\begin{frame}{Documentation and Wiki}
    \includegraphics[scale=0.2]{docu}
\end{frame}

\begin{frame}{Documentation and Wiki}
    \includegraphics[scale=0.25]{wiki}
\end{frame}


\subsection{Milestones}
\begin{frame}{Milestones}
    \includegraphics[scale=0.2]{milestones}
\end{frame}

\begin{frame}{Issues}
    \includegraphics[scale=0.2]{issues}
\end{frame}

\section{Milestones / Goals}

\subsection{Status / Milestones}

\begin{frame}{Status / Milestones}

  \textbf{Status:}
  \begin{itemize}
  \item
    First connection to the board \par
    \begin{itemize}
    \item[$-$] Getting to know the toolchain
    \end{itemize}
    $\Rightarrow$ Done
  \item
    Simulate Example from the DE10-Standard System CD \par
    \begin{itemize}
    \item[$-$] Research documentation and software
    \item[$-$] Test own changes
    \end{itemize}
    $\Rightarrow$ Done
  \end{itemize}
  
  \textbf{Next steps:}
  \begin{itemize}
  \item
    Deploy OpenMsp on Cyclone V \par
    \begin{itemize}
    \item[$-$] Handle differences Cyclone II vs Cyclone V
    \item[$-$] Gain deep knowledge on Verilog
    \item[$-$] Change files in the OpenMsp project
    \end{itemize}
    $\Rightarrow$ Two weeks - max. 4
  \end{itemize}
\end{frame}

\subsection{Milestones - next steps}
\begin{frame}{Milestones - next steps}
  \begin{itemize}
  \item
    Simulating the given examples from Sancus on the board \par
    \begin{itemize}
    \item[$-$] Handle differences between the D1-Standard Board and the D10-Standard Board
    \item[$-$] Use the toolchain given by Sancus:
      \begin{itemize}
      \item[$+$] Research and handle missing information
      \item[$+$] Handle problems caused by version-differences
      \end{itemize}
    \end{itemize}
    $\Rightarrow$ Two weeks
  \item
    Create and deploy one example on one node
    \begin{itemize}
    \item[$-$] Test different examples
    \item[$-$] Attempt to find security flaws
    \end{itemize}
    $\Rightarrow$ End of the year (december)
  \item
    Deploy multiple examples on one node
    \begin{itemize}
    \item[$-$] Two modules communicating with each other
    \item[$-$] One module trying to snoop on a another one
    \item[$-$] One module trying to leak knowledge to the other one without permissions
    \end{itemize}
    $\Rightarrow$ Two weeks
  \item
    Serial communication to a node
    \begin{itemize}
    \item[$-$] Deploying two modules on two nodes
    \item[$-$] Communication to an external device
    \end{itemize}
    $\Rightarrow$ February / end of the semester / before the presentation
  \end{itemize}
\end{frame}

\end{document}
