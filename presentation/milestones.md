## Milestones 05.12.2019

### Done

- Simulation of the different examples provided by Sancus
- Handling different toolchains on Ubuntu and Arch
    * toolchain provided by Sancus
    * Docker

- Evaluation of different hardware boards
- Ordering one board
- First connection to the board
 // within the first days - max. one week
    * Research of the toolchain
- Simulation examples from the DE10-Standard_System-CD

### Next steps

- Deploy OpenMsp on Cyclone V
// goal: two weeks - max 4 weeks
    * Handle differences between the D1-Standard Board and the D10-Standard Board
    + handle differences Cyclone II vs Cyclone V
    + gain deep knowledge on Verilog
    + change files in the OpenMsp project

- Simulating the given examples on the board
 // one - two weeks
    * Use the toolchain given by Sancus:
        + research & handle missing information
        + handle problems caused by version-differences
- Create and deploy one example on one node 
 // end of the year
    * test different examples with and without flaws
- Deploy multiple examples on one node
 // one week
    * two modules communicating with each other
    * one module trying to snoop on a another one
    * one module trying to leak knowledge to the other one without permissions
- Serial communication to a node
 // February / end of the semester / before the presentation
    * deploying two modules on two nodes
    * communication to an external device

### Vision / Best-case scenario
- Establishing a webserver on the FPGA 
    * using sancus-contiki
    * test modules communicating with each other via the server
    * test possible security vulnerabilities and flaws
- Deploy examples on two different nodes
    * two modules communicating with each other via a server









