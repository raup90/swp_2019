Mögliche Inhalte Endpräsentation:

1. Sancus [Alpha]
- Was war Sancus
- Tools & API
- Schreiben & Ausführung von Softwaremodulen
- Sancus Simulator

2. OpenMsp430 [Elena]
- Was ist OpenMsp430
- Rolle von OpenMsp430 im Sancus Projekt
- Struktur des OpenMsp Repo
- Tools
  + Quartus II generell
  + IP Catalog
  + qsys
  + DE10 Standard System Builder

3. Unsere Arbeit
3.1 Arbeit mit Sancus [Alpha]
  + Simulationen
  + Eigene Softwaremodule & Nutzung der Sanucs API

3.2. OpenMSP430 auf dem Board [Elena/Anton]
(geschafft):
- OpenMsp auf DE10 Standard Board
  + PIN Assignments
  + Änderungen im zentralen File rtl/veriolog/OpenMSP430_fpga.v
  + Ram & Rom
(nicht geschafft/ Ansätze):
- UART / Debugging
  + Versuch Reassignen der Pins
  + Nutzung von qsys
  + Nutzung des HPS
- Compilen von OpenMsp für DE10
  + Handhabung von Warnings

4. Schwierigkeiten [Elena/Anton]
- Probleme mit der Quartus II Software
  + Lizenzen
  + Überblick über die ganzen Tools & Zusammenhänge
  + Keine Erfahrung
- Probleme mit OpenMsp
  + fehlende Dokumentation
- Vergleich mit anderen Projekten die von DE1 auf DE10 umgestiegen sind

5. Reflection & lessons learned [Alle]
- Vergleich Milestones vs Status Quo
- Was ist gut gelungen
  + Regelmässige Arbeit daran
  + Ausgeglichene Teamarbeit / Keiner hat nichts gemacht
  + Neue Vorschläge immer gut aufgenommen/ Meinungen berücksichtigt
  + Gute Kommunikationen
  + Nutzen des Repos & Dokumentationen
- Was hätte man besser machen können
  + Mehr Dokumentationen lesen (z.B. Sanucus API sehr spät bemerkt)
  + Mehr Arbeitsaufteilung & Issues besser strukturieren
  + Parallel an Thema Sancus & Thema OpenMsp auf De10 arbeiten
  + Zu starker Fokus auf OpenMsp?? / Sancus vernachlässigt
  + Erst Grundlegenes Wissen aneignen z.B. eigenes kleines Projekt für DE10 schreiben fürs Verständnis
- Nutzen des Projektes
  + Persönliche Weiterbildung
  + Nutzen unserer Arbeit für die weitere Arbeit an dem Thema
