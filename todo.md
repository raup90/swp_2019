# TODO

* handle / fix existing todos and issues
* finish documentation
* write own c Modules to simulate with sancus-sim ?
* new presentation
* for each doc folder: overview.md should give an overview over all docs and subfolders 

## Documentation

* sancus-simluator: _Alpha_
* compiling a file / Quartus: _Anton_
  * how-to
  * warnings on OpenMsp
* UART on De10 Standard: _Elena_
  * Response from Terasic
  * Using qsys
    * using quartus_ipgen
    * problems
  * reassigning pins
* IP Catalog: _Anton_
* RAM / ROM: _?_
  * Memory image
  * Settings for OpenMsp430
