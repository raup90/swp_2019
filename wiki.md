# Major documentation documents

We originally intended to use this wiki as our primary place documentation for documentation,
but during our work we realized that having the documentation near the topics/items that are
documented was much easier to work with.

This is therefore merely a remnant of our previous intent, but since you came here for detailed
information, here is a list of documents that might be interesting, are the most detailed,
provide an overview of a complicated topic or simply a general introduction:

<!-- symlinking those files didn't work unfortunately, so I (hyper-)link -->
<!-- the files in main repo instead -->
<!-- TODO: is this what we want? We miss some opertunities the wiki system -->
<!-- TODO: offers (e.g. page titles, pages viewable in sidebar etc. -->
<!-- TODO: another solution would be to move general documentation entirely -->
<!-- TODO: to the wiki, keeping only internal docs in the main repo -->

* [Sancus architecture overview](https://git.imp.fu-berlin.de/raup90/swp_2019/blob/master/docs/Sancus/architecture.md) - What is Sancus all about?
* [Sancus repositories](https://git.imp.fu-berlin.de/raup90/swp_2019/blob/master/Repos/overview.md) - What is where in the Sancus GitHub repositories?
* [FPGA architecture overview](https://git.imp.fu-berlin.de/raup90/swp_2019/blob/master/docs/fpga/field_programmable_gate_arrays.md)
* [Building the toolchain on Ubuntu / Debian](https://git.imp.fu-berlin.de/raup90/swp_2019/blob/master/Repos/build_documentation_ubuntu.md)
* [Building the toolchain on Arch linux](https://git.imp.fu-berlin.de/raup90/swp_2019/blob/master/Repos/build_documentation_arch.md)
* [Building the toolchain using Docker](https://git.imp.fu-berlin.de/raup90/swp_2019/blob/master/docker/README.md)
* [Overview Altera DE10-Standard: files, tools, manuals](https://git.imp.fu-berlin.de/raup90/swp_2019/blob/master/docs/DE10-standard/overview.md)
* [Altera DE10-Standard: installation](https://git.imp.fu-berlin.de/raup90/swp_2019/blob/master/docs/DE10-standard/README.md)
