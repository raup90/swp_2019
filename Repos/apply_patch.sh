#!/bin/sh

# # Patches:
# - anton_fix_llvm_error.patch
#     On my system, when trying to build sancus-main, target 'llvm-build',
#     a conversion error in a C++ file deep down the directory tree caused the
#     build to fail. At least for me, that fixed the problem
# - anton_fix_mspgcc_version.patch
#     Apparently TI MSPGCC version 6.4.0.32 isn't online anymore, so I upgraded
#     to version 8.3.0.16

# Make sure we are in the dir the script is in,
# because the paths in the patchfiles are realtive to this dir
#     Absolute path to this script, e.g. /home/user/bin/foo.sh
script=$(readlink -f "$0")
#     Absolute path this script is in, thus /home/user/bin
scriptpath=$(dirname "$script")
cd $scriptpath

patchfile=

usage() {
      echo "usage: $0 [--unpatch] patchfile.patch"
}
do_patch() {
      # p0 means that the paths in the .patch file don't need be stripped
      #   (they are relative to the current directory)
      patch -p0 --forward < $patchfile
}
do_unpatch() {
	patch -p0 -R < $patchfile; \
}

case $1 in
      -h | --help )
            usage
            ;;
      -u | --unpatch | --undo )
            patchfile=$2
            do_unpatch
            ;;
      --patch | --apply)
            patchfile=$2
            do_patch
            ;;
      "" )
            usage
            exit 1
            ;;
      * )
            patchfile=$1
            do_patch
            ;;
esac