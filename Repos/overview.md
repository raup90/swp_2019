# Sancus - Protected Module Architecture

## Repository overview

This file should give an overview about all repositories from the Sancus developer. The description of each repository is quoted from the main page of the repositories.


### sancus-main 

_Top-level repository and build script_

In this repository contains a build script (Makefile) to create a working Sancus development environment by resolving system dependencies, and installing the latest sub-projects. The resulting Sancus distribution offers a complete development environment, including simulator, compiler/toolchain, support libraries, and example programs.

Inside the docker directory is another build script which allows to build a docker image that contains the Sancus toolchain, ready to run the example programs. The Docker script builds an Ubuntu 18.04-based container.



### sancus-core

_Minimal OpenMSP430 hardware extensions for isolation and attestation_

This repository provides source code and binary packages for the official Sancus 2.0 release: simulator, compiler and runtime support libraries. 

There are 4 relevant directories:
      - core
        openMSP430 Core top level directory, containing benchmarks, diverse documentations, RTL sources (verilog files and peripherals directory), simulations directory (RTL simulations scripts) and synthesis (synthesis setup for area & speed analysis)
      - doc
        diverse documentation, e.g. about the openMSP430 microcontroller with manual and configuration for usage
      - fpga
        openMSP430 FPGA Projects top level directory, containing the supported FPGAs with benchmarks, documentation and rtl sources
      - tools
        openMSP430 Software Development Tools top level directory, containing the main TCL scripts, Common libraries and GDB Proxy server main project directory, 


This repository will be downloaded by the build-script of the __sancus-main__ repository and installs `sancus-sim`, `sancus-loader`, `sancus-gdbproxy` and `sancus-minidebug`.



### sancus-compiler

_Secure compilation of annotated C code to protected modules._

In this directory are the sources for the tools `sancus-cc`, `sancus-ld` and `sancus-crypto`.

There's a _CMakeLists.txt_ which will be called from the __sancus-main__ repositories build script to install the tools.



### sancus-support

_Untrusted support software and device drivers_

This repository contains the source code to build neccessary Sancus libraries (e.g. the `sm_io`, `sm_control`, `malloc`, `tools` and much more) and the required header files.

In this repo is also a _CMakeLists.txt_ file which can be used from the __sancus-main__ repo to build all files in this repository.



### sancus-examples

_Examples and test suite._

There are a few programs to demonstrate and test the security properties offered by a Sancus processor:

| Security feature          | Example program | Comments                                                     |
| ------------------------- | --------------- | ------------------------------------------------------------ |
| Software module isolation | hello-world     |	Minimal working example to enable/disable a Sancus module.   |
| Secure linking            | sensor-reader   | Caller/callee authentication between sensor and reader SMs.  |
| Remote attestation 	    | sensor-reader   | Compute MAC over fresh challenge (nonce).                    |
| Secure communication      | sensor-reader   | Authenticated encryption with associated data primitive.     |
| Confidential deployment   | hello-world     | SM text section is transparently encrypted at build time.    |

The directory _sensor-reader_ contains the example from the [KU Leuven Homepage](https://distrinet.cs.kuleuven.be/software/sancus/examples.php "Sancus sensor reader example"):

> This is an end-to-end example with Sensor-reader SoftwareModules to illustrate how to build and run a Sancus application. This follows the example given in the paper: one protected module providing readings from a memory-mapped I/O sensor, plus one that transforms this data and wraps (encrypts + signs) it to be sent to the vendor.
>
> The remote stakeholder is provided with an authenticity guarantee: a good signature over sensor readings associated with a fresh nonce can only be produced by untampered sensor/reader SMs. Moreover, the confidentiality of the transformed sensor readings is preserved.

After building the Sancus toolchain this examples can be compiled and executed.



### vulcan

_VulCAN: Efficient Component Authentication and Software Isolation for Automotive Control Networks_


This repository contains the source code accompanying the paper on automotive network security which appears in the 2017 ACSAC conference.

> Jo Van Bulck, Jan Tobias Mühlberg, and Frank Piessens. 2017. VulCAN: Efficient Component Authentication and Software Isolation for Automotive Control Networks. In Proceedings of the 33th Annual Computer Security Applications Conference (ACSAC'17).

__Abstract__

Vehicular communication networks have been subject to a growing number of attacks that put the safety of passengers at risk. This resulted in millions of vehicles being recalled and lawsuits against car manufacturers. While recent standardization efforts address security, no practical solutions are implemented in current cars.

This paper presents VulCAN, a generic design for efficient vehicle message authentication, plus software component attestation and isolation using lightweight trusted computing technology. Specifically, we advance the state-of-the-art by not only protecting against network attackers, but also against substantially stronger adversaries capable of arbitrary code execution on participating electronic control units. We demonstrate the feasibility and practicality of VulCAN by implementing and evaluating two previously proposed, industry standard-compliant message authentication protocols on top of Sancus, an open-source embedded protected module architecture. Our results are promising, showing that strong, hardware-enforced security guarantees can be met with a minimal trusted computing base without violating real-time deadlines under benign conditions.

