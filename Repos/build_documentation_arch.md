# Steps i took to build/install the toolchain on arch

1. get sancus-main repo
    ```(sh)
    git clone git@github.com:sancus-pma/sancus-main.git
    ```
    or
    ```(sh)
    git clone https://github.com/sancus-pma/sancus-main.git
    ```
2. ```(sh)
   cd sancus-main`
   ```
3. ```(sh)
   make llvm-build
   ```
    * On my machine the compilation failed due to an invalid type signature
    * If that is the case for you too, I created a patch that fixes the problem
    * ```(sh)
      ../apply_patch.sh ../anton_fix_llvm_error.patch
      ```
    * (retry `make`)
4. Unfortunately the original Makefile heavily depends on those debian packages, so until we have a nicer fix, we build everything from source and pretend that we have the debian package so `make` complies
   ```(sh)
   make llvm-inst
   touch clang-sancus__amd64.deb
   touch clang-sancus
   ```
5. Building TI MSPGCC will most likely also fail, because the specified version isn't online anymore (at least when i looked). To fix this, there is another patch that adjusts the MSPGCC version to 8.3.0.16 (hoping that everything is backwards compatible)
    ```(sh)
    ../apply_patch.sh ../anton_fix_mspgcc_version.patch
    make ti-mspgcc-build
    ```
6. ```(sh)
   touch ti-mspgcc-binutils-sancus_8.3.0.16-1_amd64.deb
   make ti-mspgcc-inst
   touch ti-mspgcc
   ```
7. Install [gcc-msp430 (AUR)](https://aur.archlinux.org/packages/gcc-msp430/)
   ```(sh)
   yay -S binutils-msp430 gcc-msp430 msp430-libc msp430mcu
   touch debian-deps
   ```
8. Install python3 pip packages
   ```(sh)
   make pip-deps
   ```

## Todo
* Maybe write this as a Makefile itself
* Find out whether the llvm error occurs on other systems too. Maybe it was just my system that was badly configured
* Is `make ti-mspgcc-build` necessary? There is [msp430-elf-binutils (AUR)](https://aur.archlinux.org/packages/msp430-elf-binutils/), maybe that works too?
* Look into:
  * [https://aur.archlinux.org/packages/ti-msp430ware/]
  * [https://aur.archlinux.org/packages/python-msp430-tools/]
  * [https://aur.archlinux.org/packages/mspgcc-ti/]
  * 