# Some notes regarding the simulator `sancus-sim`

* `sancus-sim` is really the python script [sancus-core/core/sim/rtl_sim/sancus/run_sim.py](sancus-core/core/sim/rtl_sim/sancus/run_sim.py) which calls `iverilog` internally
* The files _sancus\_sim.fst_, _sim-input.bin_, _sim-output.bin_ that usually remain in a project directory after running the simulator, are not used by the simulator (`iverilog`) itself, but from the program being simulated
  * _sim-[input/ouput].bin_ are somehow used by the "File IO peripheral" ([sancus-core/core/bench/verilog/tb_openMSP430.v line 651](https://github.com/sancus-pma/sancus-core/blob/master/core/bench/verilog/tb_openMSP430.v#L651))
  * _sancus\_sim.fst_ is called `dumpfile` in the python script, usage: ???