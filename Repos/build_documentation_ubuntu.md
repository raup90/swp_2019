# Building the toolchain from source

__INFO: works on Ubuntu 18.04. when using the already patched LLVM package__

Use the following instructions to install the complete toolchain for the sancus project. The default installation path is set to _/usr/local_. To change the prefix you can add `-DCMAKE_INSTALL_PREFIX=/your/prefix` to the cmake command. 


## Simulator

To install the _sancus-sim_, _sancus-loader_, _sancus-gdbproxy_ and _sancus-minidebug_ you need the following dependencies

| Name           | Linux package | Version |
| -------------- | ------------- | ------- |
| Python         | python3       |	≥3.3   |
| Icarus Verilog | iverilog      | 	≥0.9   |
| Tcl/Tk         | tk            | 	≥8.5   |
| CMake          | cmake         | 	≥3.4.3 |


Download the sancus-core archive and install it:
```
wget https://distrinet.cs.kuleuven.be/software/sancus/downloads/sancus-core_2.0.tar.gz
tar xzf sancus-core_2.0.tar.gz
cd sancus-core_2.0
mkdir build
cd build
cmake ..
sudo make install
```


## LLVM & Compiler

Clang has to be patched to close a bug. This is necessary to use the sancus compiler. 


### Debian/Ubuntu users

You can use the already patched LLVM package:
```
wget https://distrinet.cs.kuleuven.be/software/sancus/downloads/clang-sancus_4.0.1-2_amd64.deb
dpkg -i clang-sancus_4.0.1-2_amd64.deb

```



### Alternative: Patch clang from source

__IMPORTANT: Does not work! Could not build _CGOpenMPRuntime.cpp.o_.__

To patch clang manually you can do this using the following instructions:
```
# Get the llvm source archive and extract it
wget http://releases.llvm.org/4.0.0/llvm-4.0.0.src.tar.xz
tar xf llvm-4.0.0.src.tar.xz
cd llvm-4.0.0.src/tools/
wget http://releases.llvm.org/4.0.0/cfe-4.0.0.src.tar.xz
tar xf cfe-4.0.0.src.tar.xz
mv cfe-4.0.0.src clang
cd clang
# get the patch
wget https://distrinet.cs.kuleuven.be/software/sancus/downloads/clang.patch
patch -p1 < clang.patch
cd ../..
# building
mkdir build
cd build
# cmake command does not work well
# got much errors while building `clangCodeGen.dir/CGOpenMPRuntime.cpp.o`
# so do not install every LLVM backend but only the necessary one, add
# following parameter to the cmake command
## cmake ..
cmake -DLLVM_TARGETS_TO_BUILD=MSP430 ..
# use -j4 for using 4 cpus (it takes about half an hour to hours...)
make -j4
sudo make install -j4
```


## Compiler

Now you can install the _sancus-cc_, _sancus-ld_ and _sancus-crypto_. You need the following dependencies:

| Name                |	Linux package         | Version   |
| ------------------- | --------------------- | --------- |
| Python              | python3        	      | ≥3.3      |
| pyelftools 	      | (use pip3)            | ≥0.24     |
| MSPGCC Binutils 	  | binutils-msp430 (AUR) | ≥20120406 |
| MSPGCC GCC          |	gcc-msp430 (AUR)      | ≥20120406 |
| MSPGCC libc 	      | msp430-libc (AUR)     | ≥20120242 |
| MSPGCC MCU 	      | msp430mcu (AUR)	      | ≥20120406 |
| Patched LLVM/Clang  | N/A (see above)       | N/A       |
| GNU C++ Compiler    | g++ / gcc             | ≥4.8      |
| CMake               | cmake                 | ≥3.4.3    |


Install dependency pyelftools:
```
sudo pip3 install pyelftools
```

To install the compiler follow this instructions:
```
# Get the source archive
wget https://distrinet.cs.kuleuven.be/software/sancus/downloads/sancus-compiler_2.0.tar.gz
tar xzf sancus-compiler_2.0.tar.gz
# building
cd sancus-compiler_2.0
mkdir build
cd build
cmake ..
make
make install
```

## Source

* [Sancus installation](https://distrinet.cs.kuleuven.be/software/sancus/install.php "KU Leuven Sancus installation")
