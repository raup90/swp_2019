# Violation Example

## Description

This example provided by Sancus on their [sancus-examples](https://github.com/sancus-pma/sancus-examples/tree/master/violation "sancus-examples/violation") github repository demonstrates the behaviour if software modules try to access the protected section of another Sancus-enabled software module.


## Running

Use the `Makefile` to build and run the violation example:
```bash
root@ab19158b8e32:/src/sancus_projects/violation# make sim                            
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g   -c -o main.o main.c                                                                                                              
sancus-ld -L"/usr/local/share/sancus-support"/lib/ --ram 16K --rom 41K  -lsm-io -ldev --inline-arithmetic --standalone --verbose -o no_mac_main.elf main.o                                                                          
INFO: Found new Sancus modules:                                                                                   
INFO:  * foo:                 
INFO:   - Entries: foo_entry  
INFO:   - No calls to other modules
INFO:   - No unprotected outcalls
INFO: No existing Sancus modules found
INFO: No asm Sancus modules found
INFO: Found MSP430 install directory: /usr/msp430
INFO: Found MSP430 GCC install directory: /usr/lib/gcc/msp430/4.6.3                                               
INFO: Using output file no_mac_main.elf
/usr/lib/gcc/msp430/4.6.3/../../../../msp430/bin/ld: warning: section `.bss' type changed to PROGBITS             
sancus-crypto --fill-macs --key 4078d505d82099ba --verbose -o main.elf no_mac_main.elf                            
sancus-sim --ram 16K --rom 41K  main.elf
Starting Verilog simulation. Press <Ctrl-C> to get to the Icarus Verilog CLI, then "finish" to exit.              
******************************
* Sancus simulation started  *
* ROM: 41984B                *
* RAM: 16384B                *
******************************
sim_file: /tmp/tmpw1ztzpnb
=== Spongent parameters ===                         
Rate:        18                                          
State size: 176
===========================                              
=== SpongeWrap parameters ===                            
Rate:           16                           
Security:       64     
Blocks in key:   4                   
=============================
=== File I/O ===                                                                                                                                                                                                                    
Input:  'sim-input.bin' 
Output: 'sim-output.bin'
================                                                                                                  
FST info: dumpfile sancus_sim.fst opened for output.
                                                         
------


[main.c] enabling foo SM..
New SM config: 6bf8 6d16 029c 03a6, 0
Vendor key: 4078d505d82099ba
...............................................................................................................................................
SM key: 892cafb62731a920
SM foo with ID 1 enabled        : 0x6bf8 0x6d16 0x029c 0x03a6
accessing foo private data at 0x29c
mem violation @0x029c, from 0x5c70
mem violation @0x029c, from 0x5c70
        --> SM VIOLATION DETECTED; exiting...

 ===============================================
|               SIMULATION PASSED               |
 ===============================================
```
