#include "reader_sm2.h"
#include <sancus/sm_support.h>
#include <sancus_support/sm_io.h>

DECLARE_SM(reader_sm2, 0x1312);

static sensor_data_t SM_FUNC(reader_sm2) transform_readings(sensor_data_t data)
{
    return data;
}

static int SM_FUNC(reader_sm2) outside_sm(void *p)
{
    return ( (p < (void*) &__PS(reader_sm2)) || (p >= (void*) &__PE(reader_sm2)) ) &&
           ( (p < (void*) &__SS(reader_sm2)) || (p >= (void*) &__SE(reader_sm2)) );
}

void SM_ENTRY(reader_sm2) get_readings_sm2(nonce_t no, ReaderOutput* out)
{
    /* Ensure output memory range about to be dereferenced lies outside SM. */
    ASSERT(outside_sm(out) && outside_sm(out + sizeof(ReaderOutput) - 1));

    /* Securely verify and call sensor SM. */
    sensor_data_t data = read_sensor_data();
    dump_buf((uint8_t*)&data, sizeof(sensor_data_t), "  Data from READER 2");

    /* Transform and seal sensor readings. */
    sensor_data_t transformed = transform_readings(data);
    ASSERT(sancus_wrap(&no, sizeof(no), &transformed, sizeof(transformed),
                       &out->cipher, &out->tag));
}
