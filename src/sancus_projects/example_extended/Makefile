ifndef SANCUS_DIR
  SANCUS_SUPPORT_DIR  = "/usr/local/share/sancus-support"
else
  SANCUS_SUPPORT_DIR  = ${SANCUS_DIR}/share/sancus-support
endif

ifndef SANCUS_SECURITY
  SANCUS_SECURITY   = 64
endif
ifeq ($(SANCUS_SECURITY), 64)
    SANCUS_KEY      = 31f16ada5a4399b7
else
    SANCUS_KEY      = 9a0488baf6240cd76579e964f67ac5ef
endif

CC                  = sancus-cc
LD                  = sancus-ld
SANCUS_CRYPTO       = sancus-crypto
SANCUS_SIM          = sancus-sim
SANCUS_LOAD         = sancus-loader
RM                  = rm -f

## Software Provider IDs
VENDOR_ID_SENSOR    = 1234
VENDOR_ID_1         = 6789
VENDOR_ID_2         = 1312
VENDOR_ID_MAL       = 9876

RAM_SIZE            = 16K
ROM_SIZE            = 41K
FPGA_DEV            = /dev/ttyUSB0
FPGA_RATE           = 115200

## Software Provider Keys
VENDOR_KEY_SENSOR   = $(shell $(SANCUS_CRYPTO) --key $(SANCUS_KEY) --gen-vendor-key $(VENDOR_ID_SENSOR) | xxd -p)
VENDOR_KEY_1        = $(shell $(SANCUS_CRYPTO) --key $(SANCUS_KEY) --gen-vendor-key $(VENDOR_ID_1) | xxd -p)
VENDOR_KEY_2        = $(shell $(SANCUS_CRYPTO) --key $(SANCUS_KEY) --gen-vendor-key $(VENDOR_ID_2) | xxd -p)
VENDOR_KEY_MAL      = $(shell $(SANCUS_CRYPTO) --key $(SANCUS_KEY) --gen-vendor-key $(VENDOR_ID_MAL) | xxd -p)

## Flags for compiling and linking
MEMFLAGS            = --ram $(RAM_SIZE) --rom $(ROM_SIZE)
CFLAGS              = -I$(SANCUS_SUPPORT_DIR)/include/ -Wfatal-errors -fcolor-diagnostics -Os -g
LDFLAGS             = -L$(SANCUS_SUPPORT_DIR)/lib/ $(MEMFLAGS) -lsm-io -ldev --inline-arithmetic --standalone --verbose
SIMFLAGS            = $(MEMFLAGS)
CRYPTOFLAGS         = --key $(VENDOR_KEY_SENSOR) --key $(VENDOR_KEY_1) --key $(VENDOR_KEY_2) --key $(VENDOR_KEY_MAL) --verbose
LOADFLAGS           = -device $(FPGA_DEV) -baudrate $(FPGA_RATE)

ifeq ($(QUIET),1)
    CFLAGS += -DQUIET
endif


SOURCES         = $(shell ls *.c)
OBJECTS         = $(SOURCES:.c=.o)

TARGET          = main.elf
TARGET_NO_MAC   = no_mac_$(TARGET)

## Software Module Keys
READER_SM_KEY_1   = `$(SANCUS_CRYPTO) --gen-sm-key reader_sm1 --key $(VENDOR_KEY_1) main.elf | xxd -p`
READER_SM_KEY_2   = `$(SANCUS_CRYPTO) --gen-sm-key reader_sm2 --key $(VENDOR_KEY_2) main.elf | xxd -p`

all: $(TARGET) verify-keys

$(TARGET_NO_MAC): $(OBJECTS)
	$(LD) $(LDFLAGS) -o $@ $^

$(TARGET): $(TARGET_NO_MAC)
	$(SANCUS_CRYPTO) --fill-macs $(CRYPTOFLAGS) -o $@ $<

load: $(TARGET)
	$(SANCUS_LOAD) $(LOADFLAGS) $<

sim: $(TARGET)
	unbuffer $(SANCUS_SIM) $(SIMFLAGS) $< | tee sim.out

verify-keys:
	@echo "\033[92mVerify output of reader module\033[0m"
	@echo "* Decrypt output of both reader modules (nonce, cipher and tag)"
	@echo "\033[92m** decrypt reader SM1\033[0m"
	@read -p "Please enter output of reader SM1 'NONCE CIPHER TAG SM_KEY' (space seperated): " NONCE CIPHER_1 TAG_1 SM_KEY_1; \
	$(SANCUS_CRYPTO) --unwrap $$NONCE $$CIPHER_1 $$TAG_1 --key $$SM_KEY_1
	@echo "\033[92m** decrypt reader SM2\033[0m"
	@read -p "Please enter output of reader SM2 'NONCE CIPHER TAG SM_KEY' (space seperated): " NONCE CIPHER_2 TAG_2 SM_KEY_2; \
	$(SANCUS_CRYPTO) --unwrap $$NONCE $$CIPHER_2 $$TAG_2 --key $$SM_KEY_2
	@echo "\033[92m=> Done.\033[0m"

clean:
	$(RM) $(TARGET) $(TARGET_NO_MAC) $(OBJECTS)
	rm -f sim-input.bin sim-output.bin sim.out
	rm -f *.fst *.vcd
