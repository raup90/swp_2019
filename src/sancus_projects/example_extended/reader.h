#ifndef READER_H
#define READER_H

#include <sancus/sm_support.h>
#include "sensor.h"

/* Protected data for READER 1 */
static int SM_DATA(reader_sm1) data_trans = 5555;

typedef struct
{
    char cipher[sizeof(sensor_data_t)];
    char tag[SANCUS_TAG_SIZE];
} ReaderOutput;

typedef unsigned nonce_t;

#endif
