#include "reader_sm1.h"
#include <sancus/sm_support.h>
#include <sancus_support/sm_io.h>

DECLARE_SM(reader_sm1, 0x6789);

static sensor_data_t SM_FUNC(reader_sm1) transform_readings(sensor_data_t data)
{
    return data ^ data_trans;
}

static int SM_FUNC(reader_sm1) outside_sm(void *p)
{
    return ( (p < (void*) &__PS(reader_sm1)) || (p >= (void*) &__PE(reader_sm1)) ) &&
           ( (p < (void*) &__SS(reader_sm1)) || (p >= (void*) &__SE(reader_sm1)) );
}

void SM_ENTRY(reader_sm1) get_readings_sm1(nonce_t no, ReaderOutput* out)
{
    /* Ensure output memory range about to be dereferenced lies outside SM. */
    ASSERT(outside_sm(out) && outside_sm(out + sizeof(ReaderOutput) - 1));

    /* Securely verify and call sensor SM. */
    sensor_data_t data = read_sensor_data();
    dump_buf((uint8_t*)&data, sizeof(sensor_data_t), "  Data from READER 1");

    /* Transform and seal sensor readings. */
    sensor_data_t transformed = transform_readings(data);
    ASSERT(sancus_wrap(&no, sizeof(no), &transformed, sizeof(transformed),
                       &out->cipher, &out->tag));
}
