#ifndef READER_SM1_H
#define READER_SM1_H

#include "reader.h"

extern struct SancusModule reader_sm1;

void SM_ENTRY(reader_sm1) get_readings_sm1(nonce_t no, ReaderOutput* out);

#endif
