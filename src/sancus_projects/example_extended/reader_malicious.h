#ifndef READER_MALICIOUS_H
#define READER_MALICIOUS_H

#include "reader.h"

extern struct SancusModule reader_malicious;

void SM_ENTRY(reader_malicious) tamper_readings_sm1(void);

#endif
