#include "reader_malicious.h"
#include <sancus/sm_support.h>
#include <sancus_support/sm_io.h>

DECLARE_SM(reader_malicious, 0x9876);

void SM_ENTRY(reader_malicious) tamper_readings_sm1(void)
{
    /* try to access data from SM READER 1*/
    data_trans++;

    /* If successfull, print data */
    dump_buf((uint8_t*)&data_trans, sizeof(sensor_data_t), "  Stolen and tampered data from READER 1");
}
