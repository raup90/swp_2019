# Extended Sensor-Reader Example with multiple SMs: One is malicious

## Description

This example extends the provided _sensor-reader example_ from the [sancus-examples](https://github.com/sancus-pma/sancus-examples/tree/master/sensor-reader "sancus-examples/sensor-reader") github repository.

Extending means that there are multiple SMs from different vendors. The vendor-IDs are defined in the `Makefile ` and calculate the vendor-keys and the SM-keys:
```bash
## Software Provider IDs
VENDOR_ID_SENSOR    = 1234
VENDOR_ID_1         = 6789
VENDOR_ID_2         = 1312
VENDOR_ID_MAL       = 9876
....
## Software Provider Keys
VENDOR_KEY_SENSOR   = $(shell $(SANCUS_CRYPTO) --key $(SANCUS_KEY) --gen-vendor-key $(VENDOR_ID_SENSOR) | xxd -p)
VENDOR_KEY_1        = $(shell $(SANCUS_CRYPTO) --key $(SANCUS_KEY) --gen-vendor-key $(VENDOR_ID_1) | xxd -p)
VENDOR_KEY_2        = $(shell $(SANCUS_CRYPTO) --key $(SANCUS_KEY) --gen-vendor-key $(VENDOR_ID_2) | xxd -p)
VENDOR_KEY_MAL      = $(shell $(SANCUS_CRYPTO) --key $(SANCUS_KEY) --gen-vendor-key $(VENDOR_ID_MAL) | xxd -p)
....
## Software Module Keys
READER_SM_KEY_1   = `$(SANCUS_CRYPTO) --gen-sm-key reader_sm1 --key $(VENDOR_KEY_1) main.elf | xxd -p`
READER_SM_KEY_2   = `$(SANCUS_CRYPTO) --gen-sm-key reader_sm2 --key $(VENDOR_KEY_2) main.elf | xxd -p`
```

Two of the reader-modules are requesting sensor readings from the sensor-module. The last reader-module try to leak data from one of the other software modules and performs unauthenticated access to the protected data section. This access should be denied by the Sancus architecture.

After running the simulation the received data can be decrypted using the correct software-module key.


## Building and Running

To run the example use the provided `Makefile`
```bash
root@ab19158b8e32:/src/sancus_projects/example_extended# make sim
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g   -c -o main.o main.c
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g   -c -o reader_malicious.o reader_malicious.c
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g   -c -o reader_sm1.o reader_sm1.c
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g   -c -o reader_sm2.o reader_sm2.c
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g   -c -o sensor.o sensor.c
sancus-ld -L"/usr/local/share/sancus-support"/lib/ --ram 16K --rom 41K -lsm-io -ldev --inline-arithmetic --standalone --verbose -o no_mac_main.elf main.o reader_malicious.o reader_sm1.o reader_sm2.o sensor.o
INFO: Found new Sancus modules:
INFO:  * reader_malicious:
INFO:   - Entries: tamper_readings_sm1
INFO:   - No calls to other modules
INFO:   - Unprotected calls: printf2, putchar
INFO:  * reader_sm1:
INFO:   - Entries: get_readings_sm1
INFO:   - SM calls: sensor
INFO:   - Unprotected calls: printf2, puts, putchar
INFO:  * reader_sm2:
INFO:   - Entries: get_readings_sm2
INFO:   - SM calls: sensor
INFO:   - Unprotected calls: printf2, puts, putchar
INFO: No existing Sancus modules found
INFO: Found asm MMIO Sancus modules:
INFO:  * sensor
INFO:   - Entries: read_sensor_data
INFO:   - Config: callerID=any, private data=[0x190, 0x198[
INFO: Found MSP430 install directory: /usr/msp430
INFO: Found MSP430 GCC install directory: /usr/lib/gcc/msp430/4.6.3
INFO: Using output file no_mac_main.elf
/usr/lib/gcc/msp430/4.6.3/../../../../msp430/bin/ld: warning: section `.bss' type changed to PROGBITS
sancus-crypto --fill-macs --key 8aedfda2911f294c --key 4d976b2709209e30 --key 7c3184e1a7dff67d --key 8b3af355298f8195 --verbose -o main.elf no_mac_main.elf
INFO:root:MAC of sensor used by reader_sm1: 7910b2ae7977f487
INFO:root:MAC of sensor used by reader_sm2: 7910b2ae7977f487
unbuffer sancus-sim --ram 16K --rom 41K main.elf | tee sim.out
Starting Verilog simulation. Press <Ctrl-C> to get to the Icarus Verilog CLI, then "finish" to exit.
******************************
* Sancus simulation started  *
* ROM: 41984B                *
* RAM: 16384B                *
******************************
sim_file: /tmp/tmp0vrzd40n
=== Spongent parameters ===
Rate:        18
State size: 176
===========================
=== SpongeWrap parameters ===
Rate:           16
Security:       64
Blocks in key:   4
=============================
=== File I/O ===
Input:  'sim-input.bin'
Output: 'sim-output.bin'
================

------


[main.c] enabling SENSOR with VENDOR ID: 0x1234
New SM config: 7a5c 7a9a 0190 0198, 0
Vendor key: 4078d505d82099ba
...............................
SM key: 0d508011f46f2b7a
SM sensor with ID 1 enabled     : 0x7a5c 0x7a9a 0x0190 0x0198
[main.c] enabling READER 1 with VENDOR ID: 0x6789
New SM config: 7384 771a 03cc 04da, 0
Vendor key: a149cf36b7e0dbce
....................................................................................................................................................................................................................................
....................................................................................................................................................................................................................................
...
SM key: c2ebe5712b424814
SM reader_sm1 with ID 2 enabled : 0x7384 0x771a 0x03cc 0x04da
[main.c] enabling READER 2 with VENDOR ID: 0x1312..
New SM config: 771c 7a5a 04da 05e4, 0
Vendor key: a14433339720e84c
....................................................................................................................................................................................................................................
...........................................................................................................................................................................................
SM key: 75a4d6c112e2a53e
SM reader_sm2 with ID 3 enabled : 0x771c 0x7a5a 0x04da 0x05e4
[main.c] requesting sensor readings for READER 1..
  Data from READER 1 (64 bits) is: e0a2020000000000
[main.c] requesting sensor readings for READER 2..
  Data from READER 2 (64 bits) is: 64f0020000000000
[main.c] dumping sealed output from READER 1
  Nonce (16 bits) is: cdab
  Cipher (64 bits) is: 2bf32aa746ec8866
  Tag (64 bits) is: b166be63443255c4
[main.c] dumping sealed output from READER 2..
  Nonce (16 bits) is: cdab
  Cipher (64 bits) is: 4df2f74c0331dd58
  Tag (64 bits) is: e3a892d50251243f
[main.c] all done!
[main.c] Now checkout with a malicious software module
[main.c] enabling MALICIOUS READER with VENDOR ID: 0x9876 
New SM config: 71ec 7382 02c4 03cc, 0
Vendor key: ddb1248075a10e9a
...........................................................................................................................................................................................................
SM key: e0ef65d2e27028b9
SM reader_malicious with ID 4 enabled   : 0x71ec 0x7382 0x02c4 0x03cc
[main.c] try to steal data from READER 1..
mem violation @0x03cc, from 0x7326
mem violation @0x03cc, from 0x7326
        --> SM VIOLATION DETECTED; exiting...

 ===============================================
|               SIMULATION PASSED               |
 ===============================================
```


Now you can decrypt the output of the modules with the corresponding vendor-key with `make verify-keys`:
```bash
root@ab19158b8e32:/src/sancus_projects/example_extended# make verify-keys 
Verify output of reader module
* Decrypt output of both reader modules (nonce, cipher and tag)
** decrypt reader SM1
Please enter output of reader SM1 'NONCE CIPHER TAG SM_KEY' (space seperated): cdab 2bf32aa746ec8866 b166be63443255c4 c2ebe5712b424814
e0a2020000000000
** decrypt reader SM2
Please enter output of reader SM2 'NONCE CIPHER TAG SM_KEY' (space seperated): cdab 4df2f74c0331dd58 e3a892d50251243f 75a4d6c112e2a53e
64f0020000000000
=> Done.
```

The output of this simulation will be written to the file `sim.out`
