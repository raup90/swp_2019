# Sancus Examples

## Overview

In this directory there are some examples for Sancus.

Some of them are provided by Sancus (see: [https://github.com/sancus-pma/sancus-examples](https://github.com/sancus-pma/sancus-examples "sancus-examples repository")), others are implemented by our own to test the Sancus API

Here's a quick overview about the different examples:

``` bash
.
├── example_extended    # Sancus main example extended with multiple software modules and
├── example_homepage    # Sensor-Reader example from sancus-example repository
├── fileio              # Sancus example using File Input/Output
├── gen_objdump.sh      # shell script to view the Sancus opcodes in the objdump
├── Makefile.include    # default variables for building Sancus binaries
├── minimal             # own example with minimal effort to enable Sancus
├── sensor-reader       # Sensor-Reader example from KU-Leuven Homepage
└── violation           # example demonstration for memory access violation
```

## Running the examples

Each directory contains an indepedent Sancus example with a corresponding `README.md` and a `Makefile`.

The `README.md` describes the kind of example, building instructions and how to execute it.

To build and execute an example first start the docker container:
```bash
cd ../../docker
make run
```

Inside the Docker container navigate to an example inside the `sancus_projects` directory:
```
cd sancus_projects/SOMEEXAMPLE
```

Now start the building process and simulation using the provided _Makefile_ by typing `make sim`:
```bash
root@ab19158b8e32:/src/sancus_projects/example# make sim
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g   -c -o main.o main.c
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g   -c -o reader.o reader.c
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g   -c -o sensor.o sensor.c
sancus-ld -L"/usr/local/share/sancus-support"/lib/ --ram 16K --rom 41K  -lsm-io -ldev --inline-arithmetic --standalone --verbose -o no_mac_main.elf main.o reader.o sensor.o
 ....
******************************
* Sancus simulation started  *
* ROM: 41984B                *
* RAM: 16384B                *
******************************
 ....
 ....
 ===============================================
|               SIMULATION PASSED               |
 ===============================================
```

## Simulator

The simulator runs the verilog files with `iverilog` and executes the input binary. Basically the simulator is a python-wrapper located in */usr/local/bin/sancus-sim*, used to taking care of the necessary file conversions.
A first inspection of the `sancus-sim` shows that the used commands for this simulator is located at */usr/local/share/sancus/rtl/sim/commands.f*:
```python
...
IHEX2MEM = '/usr/local/share/sancus/tools/ihex2mem.tcl'
COMMANDS = '/usr/local/share/sancus/rtl/sim/commands.f
...
```

This commands file includes all important directories and verilog files for running the openMSP430 in iverilog. These are:
- */usr/local/share/sancus/rtl*
- */usr/local/share/sancus/rtl/sim*
- */usr/local/share/sancus/rtl/sim/tb_openMSP430.v*


After argument parsing the simulator-wrapper generates an object-dump of the input ELF-file and stores the dump in the /tmp directory. This object-dump of your ELF-file will be used by `ihex2mem` to generate/convert a memory array and stored in a mem-file (see tcl-script */usr/local/share/sancus/tools/ihex2mem.tcl*):
```python
...
ihex_file = tempfile.mkstemp('.ihex')[1]
_run('msp430-objcopy', '-O', 'ihex', in_file, ihex_file)

mem_file = tempfile.mkstemp('.mem')[1]
_run(IHEX2MEM, '-ihex', ihex_file, '-out', mem_file,
               '-mem_size', str(rom_size))

```

Finally the simulator will be executed in iverilog with a lot of predefined macros:
```python
fd, sim_file = tempfile.mkstemp()
os.close(fd)
_run('iverilog', '-DMEM_DEFINED', '-DPMEM_SIZE_CUSTOM', '-DDMEM_SIZE_CUSTOM',
                 '-DPMEM_CUSTOM_SIZE={}'.format(rom_size),
                 '-DPMEM_CUSTOM_AWIDTH={}'.format(_get_awidth(rom_size)),
                 '-DDMEM_CUSTOM_SIZE={}'.format(ram_size),
                 '-DDMEM_CUSTOM_AWIDTH={}'.format(_get_awidth(ram_size)),
                 '-DPMEM_FILE="{}"'.format(mem_file),
                 '-DFILEIO_IN="{}"'.format(fileio_in),
                 '-DFILEIO_OUT="{}"'.format(fileio_out),
                 '-DDUMPFILE="{}"'.format(dumpfile),
                 '-f', COMMANDS, '-o', sim_file)

print('Starting Verilog simulation. Press <Ctrl-C> to get to the Icarus '
      'Verilog CLI, then "finish" to exit.')

env = os.environ.copy()
env['IVERILOG_DUMPER'] = dumper
os.execle(sim_file, sim_file, env)
```

During execution of the `sanus-sim`, the simulation will be written in the file *sancus_sim.fst*. This file can be used to debug the simulation models. Therefor the tool `gtkwave` can be used to perform debugging on Verilog or VHDL simulation models. With the exception of interactive VCD viewing, it is not intended to be run interactively with simulation, but instead relies on a post-mortem approach through the use of dumpfiles.  Various dumpfile formats are supported, also FST:
- FST: Fast Signal Trace.  This format is a block-based variant of IDX which is designed for very fast sequential and random access.


### Debugging the simulation

For Verilog, GTKWave allows users to debug simulation results at both the net level by providing a bird's eye view of multiple signal values over varying periodsof time and also at the RTL level through annotation of signal values back into the RTL for a given timestep.  The RTL browser frees up users from needing to be concerned with the actual location of where a given module resides in the RTL as the view provided by the RTL browser defaults to the module level.  This provides quick access to modules in the RTL as navigation has been reduced simply to moving up and down in a tree structure that represents the actual design.
```
$ gtkwave sancus_sim.fst
```

![sancus enabled openMSP430 in GTKWave][gtkwave]

There are also some CLI tools for converting and inspect the simulation models, i. e.:
```
$ fst2vcd sancus_sim.fst
$ fstminter sancus_sim.fst
```

### FileIO

After running the simulation there are additional files in the directory:
* sim-input.bin
* sim-output.bin

This files are for simulating file operations with Sancus. In the standard example the files are empty, because there is no file processing.

Like described before, the `sancus-sim` calls `iverilog` to generate the executable _sim_file_ with a few predefined macros. Two of these macros are for file processing:

* `FILEIO_IN=sim_input.bin`
* `FILEIO_OUT=sim_output.bin`

As you can see in _/usr/local/share/sancus/rtl/sim/file_io.v_ that this macros can be used to read input from the file _sim_input.bin_ and write the output to _sim_output.bin_:
```
`ifdef FILEIO_IN
    in_file = $fopen(`FILEIO_IN, "rb+");
    if (in_file == 0)
    begin
        $display("Fail: unable to open '%s' for reading", `FILEIO_IN);
        $finish;
    end
    $display("Input:  '%s'", `FILEIO_IN);
`endif

`ifdef FILEIO_OUT
    out_file = $fopen(`FILEIO_OUT, "wb+");
    if (out_file == 0)
    begin
        $display("Fail: unable to open '%s' for writing", `FILEIO_OUT);
        $finish;
    end
    $display("Output: '%s'", `FILEIO_OUT);
`endif
```

There is also an example for [file input/output](https://github.com/sancus-pma/sancus-examples/tree/master/fileio "fileio example") provided by Sancus. Here is an output from the running simulator:
```
=== File I/O ===
Input:  'sim-input.bin'
Output: 'sim-output.bin'
================
FST info: dumpfile sancus_sim.fst opened for output.

------


[fileio.c] dumped '0x68' on fileio_out
[fileio.c] dumped '0x65' on fileio_out
[fileio.c] dumped '0x6c' on fileio_out
[fileio.c] dumped '0x6c' on fileio_out
[fileio.c] dumped '0x6f' on fileio_out
 ===============================================
|               SIMULATION PASSED               |
 ===============================================
sim output is: 'hello'
```

## Source

* [Sancus example](https://distrinet.cs.kuleuven.be/software/sancus/examples.php "KU Leuven Sancus examples")
* [Sancus documentation](https://distrinet.cs.kuleuven.be/software/sancus/doc.php "KU Leuven Sancus documentation")
* [GTKWave](http://gtkwave.sourceforge.net/gtkwave.pdf "GTKWave Manual")


[gtkwave]: gtkwave.png "sancus enabled openMSP430 in GTKWave"
