#include <msp430.h>
#include <stdio.h>
#include <sancus/sm_support.h>
#include <sancus_support/sm_io.h>

DECLARE_SM(my_sm, 0xbaff);

void SM_ENTRY(my_sm) hello_entry(void) {
    pr_info2("hi from my_sm with ID %d, called by %d\n",
        sancus_get_self_id(), sancus_get_caller_id());
}

int main() {
    msp430_io_init();
    pr_info2("hi from main with ID %d, called by %d\n",
        sancus_get_self_id(), sancus_get_caller_id());

    pr_info("enabling my_sm");
    sancus_enable(&my_sm);

    pr_info("sm_info");
    pr_sm_info(&my_sm);

    pr_info("calling my_sm");
    hello_entry();

    pr_info("back in main");

    return 0;
}
