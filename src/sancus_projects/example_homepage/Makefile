include ../Makefile.include

## Overwrite default values
ROM				= 48K
RAM				= 10K
STACK			= 256
VENDOR_ID		= 1234
SANCUS_KEY		= deadbeefcafebabe
VENDOR_KEY      = $(shell $(SANCUS_CRYPTO) --key $(SANCUS_KEY) --gen-vendor-key $(VENDOR_ID) | xxd -p)

SOURCES         = $(shell ls *.c)
OBJECTS         = $(SOURCES:.c=.o)

TARGET          = main.elf
TARGET_NO_MAC   = no_mac_$(TARGET)



all: $(TARGET)

%.o: %.c
	@echo "\033[92m** Compiling sources\033[0m"
	$(CC) $(CFLAGS) -c $< -o $@

$(TARGET_NO_MAC): $(OBJECTS)
	@echo "\033[92m** Linking binaries\033[0m"
	$(LD) $(LDFLAGS) -o $@ $^

$(TARGET): $(TARGET_NO_MAC)
	@echo "\033[92m** Fill in the hash section using calculated key\033[0m"
	$(SANCUS_CRYPTO) --fill-macs --key $(VENDOR_KEY) -o $@ $<
	@echo "\033[92m=> finished building binaries\033[0m"

sim: $(TARGET)
	@echo "\033[92mStart simulation\033[0m"
	$(SANCUS_SIM) $(SIMFLAGS) $<
	@echo "\033[92m=> Simulation finished.\033[0m"

verify-keys:
	@echo "\033[92mVerify output of reader module\033[0m"
	@echo "\033[92m** Decrypt output of reader module (nonce, cipher and tag)\033[0m"
	@read -p "Please enter output of reader 'NONCE CIPHER TAG SM_KEY' (space seperated): " NONCE CIPHER TAG SM_KEY; \
	$(SANCUS_CRYPTO) --unwrap $$NONCE $$CIPHER $$TAG --key $$SM_KEY
	@echo "\033[92m=> Done.\033[0m"

clean:
	@echo "\033[92mCleaning up\033[0m"
	$(RM) $(TARGET) $(TARGET_NO_MAC) $(OBJECTS)
	rm -f sim-input.bin sim-output.bin sim.out
	rm -f *.fst *.vcd
	@echo "\033[92m=> Done.\033[0m"
