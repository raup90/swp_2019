# Sensor-Reader Example (KU Leuven Homepage)

## Description

This is the example provided by Sancus on the [homepage of KU Leuven](https://distrinet.cs.kuleuven.be/software/sancus/examples.php "KU Leuven Homepage - Sanucs example").

This is a complete example of how to build and run a Sancus application. The source code follows the example in the given paper: 
> one protected module providing sensor data and one that transforms this data and signs it to be sent to the vendor.

This example is similar to the updated example scenario with an MMIO sensor module providing authenticated readings from a time stamp counter peripheral (see: [../sensor-reader](../sensor-reader/ "updated sensor-reader example"))

## Building

Compile source files:
```bash
root@ab19158b8e32:/src/sancus_projects/example_homepage# sancus-cc -I$/usr/local/share/sancus-support/include/ -Wfatal-errors -fcolor-diagnostics -Os -g --verbose -c -o sensor.o sensor.c 
INFO: Using output file sensor.o
INFO: Found MSP430 install directory: /usr/msp430
INFO: Found MSP430 GCC install directory: /usr/lib/gcc/msp430/4.6.3
INFO: Using include paths: /usr/lib/gcc/msp430/4.6.3/include, /usr/msp430/include, /usr/local/share/sancus-compiler/include
root@ab19158b8e32:/src/sancus_projects/example_homepage# sancus-cc -I$/usr/local/share/sancus-support/include/ -Wfatal-errors -fcolor-diagnostics -Os -g --verbose -c -o reader.o reader.c 
INFO: Using output file reader.o
INFO: Found MSP430 install directory: /usr/msp430
INFO: Found MSP430 GCC install directory: /usr/lib/gcc/msp430/4.6.3
INFO: Using include paths: /usr/lib/gcc/msp430/4.6.3/include, /usr/msp430/include, /usr/local/share/sancus-compiler/include
root@ab19158b8e32:/src/sancus_projects/example_homepage# sancus-cc -I$/usr/local/share/sancus-support/include/ -Wfatal-errors -fcolor-diagnostics -Os -g --verbose -c -o main.o main.c
INFO: Using output file main.o
INFO: Found MSP430 install directory: /usr/msp430
INFO: Found MSP430 GCC install directory: /usr/lib/gcc/msp430/4.6.3
INFO: Using include paths: /usr/lib/gcc/msp430/4.6.3/include, /usr/msp430/include, /usr/local/share/sancus-compiler/include
```

Link objects:
```bash
root@ab19158b8e32:/src/sancus_projects/example_homepage# sancus-ld -L/usr/local/share/sancus-support/lib/ -lsm-io -ldev --inline-arithmetic --standalone --rom-size 48K --ram-size 10K --sm-stack-size 256 --verbose -o main-no-mac.elf main.o reader
INFO: Found new Sancus modules:
INFO:  * sensor:
INFO:   - Entries: read_sensor_data_t
INFO:   - No calls to other modules
INFO:   - No unprotected outcalls
INFO:  * reader:
INFO:   - Entries: get_readings
INFO:   - SM calls: sensor
INFO:   - Unprotected calls: puts
INFO: No existing Sancus modules found
INFO: No asm Sancus modules found
INFO: Found MSP430 install directory: /usr/msp430
INFO: Found MSP430 GCC install directory: /usr/lib/gcc/msp430/4.6.3
INFO: Using output file main-no-mac.elf
/usr/lib/gcc/msp430/4.6.3/../../../../msp430/bin/ld: warning: section `.bss' type changed to PROGBITS
```

Then we need to make sure the MAC sections are filled in order for the reader module to be able to verify the sensor module. The first step is to calculate the vendor key:
```bash
root@ab19158b8e32:/src/sancus_projects/example_homepage# sancus-crypto --gen-vendor-key 1234 --key deadbeefcafebabe
4078d505d82099ba
```

 Then we fill in the hash sections using this key:
```bash
root@ab19158b8e32:/src/sancus_projects/example_homepage# sancus-crypto --fill-macs --key 4078d505d82099ba -o main.elf main-no-mac.elf
```

## Running

Now you can run the simulator:
```bash
root@ab19158b8e32:/src/sancus_projects/example_homepage# sancus-sim --rom-size 48K --ram-size 10K main.elf
Starting Verilog simulation. Press <Ctrl-C> to get to the Icarus Verilog CLI, then "finish" to exit.
******************************
* Sancus simulation started  *
* ROM: 49152B                *
* RAM: 10240B                *
******************************
sim_file: /tmp/tmpj506asa8
=== Spongent parameters ===
Rate:        18
State size: 176
===========================
=== SpongeWrap parameters ===
Rate:           16
Security:       64
Blocks in key:   4
=============================
=== File I/O ===
Input:  'sim-input.bin'
Output: 'sim-output.bin'
================
FST info: dumpfile sancus_sim.fst opened for output.

------


New SM config: 50d0 51ee 02a8 03b2, 0
Vendor key: 4078d505d82099ba
...............................................................................................................................................
SM key: cf1af684349e2bfa
New SM config: 51f0 5472 03b2 04bc, 0
Vendor key: 4078d505d82099ba
.................................................................................................................................................................................................................................................................................................................................
SM key: 006b1e8ce6fb556b
Nonce: cdab, Cipher: 58b6, Tag: ae160709f4d2e481
 ===============================================
|               SIMULATION PASSED               |
 ===============================================
```

After successfull completion of the simulation ýou can decrypt the output of the reader module using the `NONCE`, `CIPHER` and `TAG` from the simulations output:
```bash
root@ab19158b8e32:/src/sancus_projects/example_homepage# sancus-crypto --unwrap cdab 58b6 ae160709f4d2e481 --key 006b1e8ce6fb556b
feca
```

## Makefile

You can also use our `Makefile` for both building and running this example:
```bash
root@ab19158b8e32:/src/sancus_projects/example_homepage# make 
** Compiling sources
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g -c main.c -o main.o
** Compiling sources
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g -c reader.c -o reader.o
** Compiling sources
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g -c sensor.c -o sensor.o
** Linking binaries
sancus-ld -L"/usr/local/share/sancus-support"/lib/ --ram 16K --rom 41K  -lsm-io -ldev --inline-arithmetic --standalone --verbose -o no_mac_main.elf main.o reader.o sensor.o
INFO: Found new Sancus modules:
INFO:  * reader:
INFO:   - Entries: get_readings
INFO:   - SM calls: sensor
INFO:   - Unprotected calls: puts
INFO:  * sensor:
INFO:   - Entries: read_sensor_data_t
INFO:   - No calls to other modules
INFO:   - No unprotected outcalls
INFO: No existing Sancus modules found
INFO: No asm Sancus modules found
INFO: Found MSP430 install directory: /usr/msp430
INFO: Found MSP430 GCC install directory: /usr/lib/gcc/msp430/4.6.3
INFO: Using output file no_mac_main.elf
/usr/lib/gcc/msp430/4.6.3/../../../../msp430/bin/ld: warning: section `.bss' type changed to PROGBITS
** Fill in the hash section using calculated key
sancus-crypto --fill-macs --key 4078d505d82099ba -o main.elf no_mac_main.elf
=> finished building binaries
```

For running the simulation, type `make sim`:
```bash
root@ab19158b8e32:/src/sancus_projects/example_homepage# make sim
Start simulation
sancus-sim --ram 16K --rom 41K  main.elf
Starting Verilog simulation. Press <Ctrl-C> to get to the Icarus Verilog CLI, then "finish" to exit.
******************************
* Sancus simulation started  *
* ROM: 41984B                *
* RAM: 16384B                *
******************************
sim_file: /tmp/tmpagc22ogu
=== Spongent parameters ===
Rate:        18
State size: 176
===========================
=== SpongeWrap parameters ===
Rate:           16
Security:       64
Blocks in key:   4
=============================
=== File I/O ===
Input:  'sim-input.bin'
Output: 'sim-output.bin'
================
FST info: dumpfile sancus_sim.fst opened for output.

------


New SM config: 6f54 7072 03b2 04bc, 0
Vendor key: 4078d505d82099ba
...............................................................................................................................................
SM key: 655a035fa5200a61
New SM config: 6cd0 6f52 02a8 03b2, 0
Vendor key: 4078d505d82099ba
.................................................................................................................................................................................................................................................................................................................................
SM key: 288a533f595b2e47
Nonce: cdab, Cipher: 11d4, Tag: 6d835c50c30f1bb4
 ===============================================
|               SIMULATION PASSED               |
 ===============================================
=> Simulation finished.
```

And at least verify the output of the _reader module_ with `make verify-keys`:
```bash
root@ab19158b8e32:/src/sancus_projects/example_homepage# make verify-keys
Verify output of reader module
** Decrypt output of reader module (nonce, cipher and tag)
Please enter output of reader 'NONCE CIPHER TAG SM_KEY' (space seperated): cdab 11d4 6d835c50c30f1bb4 288a533f595b2e47
feca
=> Done.
```
