# Sensor-Reader SMs: An end-to-end example

## Description

This example is provided by Sancus on their [sancus-examples](https://github.com/sancus-pma/sancus-examples/blob/master/sensor-reader "sancus-examples/sensor-reader") github repository.

Extract of the examples `README.md`:

> This example program illustrates how to build and run a Sancus application. We follow the example given in the paper: one protected module providing readings from a memory-mapped I/O sensor, plus one that transforms this data and wraps (encrypts + signs) it to be sent to the vendor.
> 
> The remote stakeholder is provided with an *authenticity* guarantee: a good signature over sensor readings associated with a fresh nonce can only be produced by untampered sensor/reader SMs. Moreover, the *confidentiality* of the transformed sensor readings is preserved.
>


## Building and Running

Extract of the examples `README.md`:

> Complete instructions are available on the [Sancus website](https://distrinet.cs.kuleuven.be/software/sancus/examples.php). To build the program, simulate the resulting binary, and verify its output, simply run:
>
```bash
$ make clean sim
```
>

This is the output of the simulation:
```bash
root@ab19158b8e32:/src/sancus_projects/sensor-reader# make clean sim
rm -f main.elf no_mac_main.elf main.o reader.o sensor.o
rm -f sim-input.bin sim-output.bin sim.out
rm -f *.fst *.vcd
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g   -c -o main.o main.c
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g   -c -o reader.o reader.c
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g   -c -o sensor.o sensor.c
sancus-ld -L"/usr/local/share/sancus-support"/lib/ --ram 16K --rom 41K  -lsm-io -ldev --inline-arithmetic --standalone --verbose -o no_mac_main.elf main.o reader.o sensor.o
INFO: Found new Sancus modules:
INFO:  * reader:
INFO:   - Entries: get_readings
INFO:   - SM calls: sensor
INFO:   - Unprotected calls: puts, printf2, putchar
INFO: No existing Sancus modules found 
INFO: Found asm MMIO Sancus modules:
INFO:  * sensor
INFO:   - Entries: read_sensor_data
INFO:   - Config: callerID=any, private data=[0x190, 0x198[
INFO: Found MSP430 install directory: /usr/msp430
INFO: Found MSP430 GCC install directory: /usr/lib/gcc/msp430/4.6.3
INFO: Using output file no_mac_main.elf
sancus-crypto --fill-macs --key 4078d505d82099ba --verbose -o main.elf no_mac_main.elf
INFO:root:MAC of sensor used by reader: 8251dbb52709e3d8unbuffer sancus-sim --ram 16K --rom 41K  main.elf | tee sim.out
Starting Verilog simulation. Press <Ctrl-C> to get to the Icarus Verilog CLI, then "finish" to exit.
******************************
* Sancus simulation started  *
* ROM: 41984B                *
* RAM: 16384B                *
******************************
sim_file: /tmp/tmpel9orvle
=== Spongent parameters ===
Rate:        18
State size: 176
===========================
=== SpongeWrap parameters ===
Rate:           16
Security:       64
Blocks in key:   4
=============================
=== File I/O ===
Input:  'sim-input.bin'
Output: 'sim-output.bin'
================
FST info: dumpfile sancus_sim.fst opened for output.

------


[main.c] enabling sensor/reader SMs..
New SM config: 71ac 71ea 0190 0198, 0
Vendor key: 4078d505d82099ba
...............................
SM key: 47bb1e471b031260
SM sensor with ID 1 enabled     : 0x71ac 0x71ea 0x0190 0x0198
New SM config: 6e6c 71aa 02a8 03b2, 0
Vendor key: 4078d505d82099ba
...............................................................................................................................................................................................................................................................................................................................................................................................................................
SM key: 35c7ae36ecfcc8bc
SM reader with ID 2 enabled     : 0x6e6c 0x71aa 0x02a8 0x03b2
[main.c] requesting sensor readings..
  Data (64 bits) is: 566c010000000000
[main.c] dumping sealed output from reader SM..
  Nonce (16 bits) is: cdab
  Cipher (64 bits) is: d58d11f56b129be0
  Tag (64 bits) is: dda06530e39724cb
[main.c] all done!
 ===============================================
|               SIMULATION PASSED               |
 ===============================================
./unwrap.py `sancus-crypto --gen-sm-key reader --key 4078d505d82099ba main.elf | xxd -p` sim.out
./unwrap.py: sancus-crypto --unwrap cdab d58d11f56b129be0 dda06530e39724cb --key 35c7ae36ecfcc8bc
./unwrap.py: authentic sensor reading: 566c010000000000
```
