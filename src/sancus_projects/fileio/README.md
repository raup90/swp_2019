# File Input/Output Example

## Description

This example is provided by Sancus on their [sancus-examples](https://github.com/sancus-pma/sancus-examples/tree/master/fileio "sancus-examples/fileio") github repository.

This example is for simulating file operations with Sancus.

After running the simulation there are additional files in the directory:
* sim-input.bin
* sim-output.bin

The simulation processes this files and reads input from `sim-input.bin` and write the content to `sim-output.bin`. Note the `-DTEST_DATA=` flag in the build-process. This string will be parsed for simulating the input.


## Running

This is the output of the fileio example:

```bash
root@ab19158b8e32:/src/sancus_projects/fileio# make sim
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g -DTEST_DATA=\""hello"\"   -c -o fileio.o fileio.c
sancus-ld -L"/usr/local/share/sancus-support"/lib/ --ram 16K --rom 41K  -lsm-io -ldev --inline-arithmetic --standalone --verbose -ldev-fileio -o no_mac_main.elf fileio.o
INFO: No new Sancus modules found
INFO: No existing Sancus modules found
INFO: No asm Sancus modules found
INFO: Found MSP430 install directory: /usr/msp430
INFO: Found MSP430 GCC install directory: /usr/lib/gcc/msp430/4.6.3
INFO: Using output file no_mac_main.elf
sancus-crypto --fill-macs --key 4078d505d82099ba --verbose -o main.elf no_mac_main.elf
sancus-sim --ram 16K --rom 41K  main.elf
Starting Verilog simulation. Press <Ctrl-C> to get to the Icarus Verilog CLI, then "finish" to exit.
******************************
* Sancus simulation started  *
* ROM: 41984B                *
* RAM: 16384B                *
******************************
sim_file: /tmp/tmp5sayh_64
=== Spongent parameters ===
Rate:        18
State size: 176
===========================
=== SpongeWrap parameters ===
Rate:           16
Security:       64
Blocks in key:   4
=============================
=== File I/O ===
Input:  'sim-input.bin'
Output: 'sim-output.bin'
================
FST info: dumpfile sancus_sim.fst opened for output.
                                                         
------ 


[fileio.c] dumped '0x68' on fileio_out
[fileio.c] dumped '0x65' on fileio_out
[fileio.c] dumped '0x6c' on fileio_out
[fileio.c] dumped '0x6c' on fileio_out
[fileio.c] dumped '0x6f' on fileio_out
 ===============================================
|               SIMULATION PASSED               |
 ===============================================
sim output is: 'hello'
```
